<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>MAHASISWA - Learn-Tara</title>
	<!-- General CSS Files -->
	<link rel="icon" href="<?= base_url('assets2/') ?>img/favicon.png" type="image/png">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500;700;900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:500,600,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
	<!-- Template CSS -->
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>stisla-assets/css/style.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>stisla-assets/css/components.css">
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.0/dist/sweetalert2.all.min.js"></script>


	<link rel="stylesheet" href="<?= base_url("assets/css/bootstrap.min.css") ?>" />
	<link rel="stylesheet" href="<?= base_url("assets/css/bootstrap-responsive.min.css") ?>" />

	<link rel="stylesheet" href="<?= base_url("assets/css/uniform.css") ?>" />
	<link rel="stylesheet" href="<?= base_url("assets/css/select2.css") ?>" />

	<link rel="stylesheet" href="<?= base_url("assets/css/fullcalendar.css") ?>" />
	<link rel="stylesheet" href="<?= base_url("assets/css/matrix-style.css") ?>" />
	<link rel="stylesheet" href="<?= base_url("assets/css/matrix-media.css") ?>" />
	<link href="<?= base_url("assets/font-awesome/css/font-awesome.css") ?>" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url("assets/css/jquery.gritter.css") ?>" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?= base_url("assets/css/custom.css") ?>" />
	<link rel="stylesheet" href="<?= base_url("assets/css/bootstrap-wysihtml5.css") ?>" />

	<script src="<?= base_url("assets/js/excanvas.min.js") ?>"></script>
	<script src="<?= base_url("assets/js/jquery.min.js") ?>"></script>
	<script src="<?= base_url("assets/js/jquery.ui.custom.js") ?>"></script>
	<script src="<?= base_url("assets/js/bootstrap.min.js") ?>"></script>

	<script src="<?= base_url("assets/js/jquery.uniform.js") ?>"></script>
	<script src="<?= base_url("assets/js/select2.min.js") ?>"></script>