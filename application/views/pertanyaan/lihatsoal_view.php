<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card-body">
					<h2 class="card-title">Manage Soal Pembelajaran Online </h2>
					<hr>
					<a href="<?= base_url('banksoal') ?>" class="btn btn-outline-primary">
						Bank Soal</a>
					<a href="<?= base_url('soalterkirim') ?>" class="btn btn-outline-primary">
						Soal Terkirim</a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->load->view('components/foot'); ?>
<script src="assets/js/app/myfunction.js"></script>
<script src="assets/js/app/banksoal.js"></script>
<?php $this->load->view('components/jsfoot2'); ?>
</body>

</html>