<?php $this->load->view('components/head2'); ?>
<base href="<?= base_url() ?>">
<?php $this->load->view('components/navbaradmin'); ?>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;">Dashboard</h1>
        </div>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="page-header">
                    <h1><span class="icon-briefcase"></span>
                        Forum Diskusi <small>Judul Topik</small></h1>
                </div>
                <div class="panel panel-success">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <h4>Forum Diskusi Kelas TI - P1 </h4>
                        <h5>Matakuliah : Algoritma</h5>
                    </div>
                    <div class="panel-body">
                        <hr>
                        <table>
                            <tr>
                                <td><img class="imgforum" src="assets/img/avatar.jpg" alt=""></td>
                                <td class="boxkomen"><textarea name="" id="" cols="95" rows="6" disabled></textarea></td>
                            </tr>
                            <tr>
                                <td class="profilename">AL AZMI</td>
                            </tr>
                        </table>
                        <hr>
                        <table>
                            <tr>
                                <td><img class="imgforum" src="assets/img/avatar.jpg" alt=""></td>
                                <td class="boxkomen"><textarea name="" id="" cols="95" rows="6" disabled></textarea></td>
                            </tr>
                            <tr>
                                <td class="profilename">AL AZMI</td>
                            </tr>
                        </table>
                        <hr>
                        <table>
                            <tr valign="top">
                                <td width="150px">&nbsp;</td>
                                <td width="175px"><strong>Komentar</strong></td>
                                <td><textarea name="komen" cols="99%" rows="5"></textarea>
                                    <!-- <input name="" value="1832" > -->
                                </td>

                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <hr>
                                    <button class="btn btn-success pull-right">
                                        <span class="icon-floppy-disk"></span> Kirim</button>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('components/jsfoot2'); ?>