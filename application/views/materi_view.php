<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>
<!--main-container-part-->
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card-body">
					<h2 class="card-title">Materi </h2>
					<hr>
					<a href="<?= base_url('lihatup') ?>" class="btn btn-outline-primary">
						Materi PDF</a>
					<a href="<?= base_url('lihatdis') ?>" class="btn btn-outline-primary">
						Materi Diskusi</a>
				</div>
			</div>
			<!-- <button class="btn btn-success" onclick="location.href='subuploadmateri'" style="float: right;">Tambah Materi</button> -->
			<!-- <div class="row">
				<div class="col-12">
					<div class="accordion" id="collapsefilter">
						<div class="accordion-group" style="width:400px;">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#collapsefilter" href="#tampilfilter">
									<strong>Filter Data <span class="icon icon-chevron-down"></span></strong>
								</a>
							</div>
							<div id="tampilfilter" class="accordion-body collapse in">
								<div class="accordion-inner">
									<form class="form-inline">
										<table style="width:400px;">
											<tr>
												<td><label for="matakuliahfil">Matakuliah</label></td>
												<td>
													<select name="matakuliahfil" id="matakuliahfil" class="span11">
														<option value="" disabled selected>Pilih</option>
														<?php
														$namamk = $this->db->query("select * from tblmatakuliah");
														foreach ($namamk->result() as $row) {
															echo "<option value='" . $row->kodemk . "'>" . $row->namamk . " - " . $row->prodi . " - " . $row->semester . "</option>";
														}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td>&nbsp</td>
												<td>
													<button type="button" id="filter" class="btn">Filter</button>
													<button type="submit" id="filterall" class="btn">Tampil Semua</button>
												</td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover" style="width:100%;float:right;">
									<thead>
										<tr>
											<th style="width:50px;" align='center'>ID Materi</th>
											<th style="width:auto;">Matakuliah</th>
											<th>Judul Materi</th>
											<th>Tanggal</th>
											<th>File</th>
											<th colspan="2">Action</th>
										</tr>
									</thead>
									<tbody id="tblup">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div> -->
		</div>






		<?php $this->load->view('components/foot'); ?>
		<script src="assets/js/app/myfunction.js"></script>
		<script src="assets/js/app/lihatup.js"></script>

		<?php $this->load->view('components/jsfoot2'); ?>

		</body>

		</html>