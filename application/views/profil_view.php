<?php $this->load->view('components/head2'); ?>
<base href="<?= base_url() ?>">
<?php $this->load->view('components/navbaradmin'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="row mt-sm-4">
				<div class="col-12 col-md-12 col-lg-4">
					<div class="card author-box">
						<div class="card-body">
							<div class="author-box-center">
								<img alt="image" src="<?= base_url("assets/img/fotomahasiswa/" . $this->session->userdata("ses_id") . ".jpg") ?>" class="rounded-circle author-box-picture">
								<div class="clearfix"></div>
								<div class="author-box-name">
									<a href="#"><?= $profil->namaadmin ?></a>
								</div>
								<div class="author-box-job">Web Developer</div>
							</div>
							<div class="text-center">
								<div class="author-box-description">
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur voluptatum alias molestias
										minus quod dignissimos.
									</p>
								</div>
								<div class="mb-2 mt-3">
									<div class="text-small font-weight-bold">Follow Hasan On</div>
								</div>
								<a href="#" class="btn btn-social-icon mr-1 btn-facebook">
									<i class="fab fa-facebook-f"></i>
								</a>
								<a href="#" class="btn btn-social-icon mr-1 btn-twitter">
									<i class="fab fa-twitter"></i>
								</a>
								<a href="#" class="btn btn-social-icon mr-1 btn-github">
									<i class="fab fa-github"></i>
								</a>
								<a href="#" class="btn btn-social-icon mr-1 btn-instagram">
									<i class="fab fa-instagram"></i>
								</a>
								<div class="w-100 d-sm-none"></div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h4>Personal Details</h4>
						</div>
						<div class="card-body">
							<div class="py-4">
								<p class="clearfix">
									<span class="float-left">
										ID
									</span>
									<span class="float-right text-muted">
										<?= $profil->idadmin ?>
									</span>
								</p>
								<p class="clearfix">
									<span class="float-left">
										Birthday
									</span>
									<span class="float-right text-muted">
										<?= $profil->tanggallahir ?>
									</span>
								</p>
								<p class="clearfix">
									<span class="float-left">
										Phone
									</span>
									<span class="float-right text-muted">
										<?= $profil->telepon ?>
									</span>
								</p>
								<p class="clearfix">
									<span class="float-left">
										Mail
									</span>
									<span class="float-right text-muted">
										<?= $profil->email ?>
									</span>
								</p>
								<p class="clearfix">
									<span class="float-left">
										Jenis Kelamin
									</span>
									<span class="float-right text-muted">
										<?= $profil->jekel ?>
									</span>
								</p>
								<p class="clearfix">
									<span class="float-left">
										Agama
									</span>
									<span class="float-right text-muted">
										<?= $profil->agama ?>
									</span>
								</p>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h4>Skills</h4>
						</div>
						<div class="card-body">
							<ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
								<li class="media">
									<div class="media-body">
										<div class="media-title">Java</div>
									</div>
									<div class="media-progressbar p-t-10">
										<div class="progress" data-height="6">
											<div class="progress-bar bg-primary" data-width="70%"></div>
										</div>
									</div>
								</li>
								<li class="media">
									<div class="media-body">
										<div class="media-title">Web Design</div>
									</div>
									<div class="media-progressbar p-t-10">
										<div class="progress" data-height="6">
											<div class="progress-bar bg-warning" data-width="80%"></div>
										</div>
									</div>
								</li>
								<li class="media">
									<div class="media-body">
										<div class="media-title">Photoshop</div>
									</div>
									<div class="media-progressbar p-t-10">
										<div class="progress" data-height="6">
											<div class="progress-bar bg-green" data-width="48%"></div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-12 col-lg-8">
					<div class="card">
						<div class="padding-20">
							<ul class="nav nav-tabs" id="myTab2" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">About</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Setting</a>
								</li>
							</ul>
							<div class="tab-content tab-bordered" id="myTab3Content">
								<div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
									<div class="row">
										<div class="col-md-3 col-6 b-r">
											<strong>Full Name</strong>
											<br>
											<p class="text-muted"><?= $profil->namaadmin ?></p>
										</div>
										<div class="col-md-3 col-6 b-r">
											<strong>Mobile</strong>
											<br>
											<p class="text-muted"><?= $profil->telepon ?></p>
										</div>
										<div class="col-md-3 col-6 b-r">
											<strong>Email</strong>
											<br>
											<p class="text-muted"><?= $profil->email ?></p>
										</div>
										<div class="col-md-3 col-6">
											<strong>Location</strong>
											<br>
											<p class="text-muted"><?= $profil->alamat ?></p>
										</div>
									</div>
									<p class="m-t-30">Completed my graduation in Arts from the well known and
										renowned institution
										of India – SARDAR PATEL ARTS COLLEGE, BARODA in 2000-01, which was
										affiliated
										to M.S. University. I ranker in University exams from the same
										university
										from 1996-01.</p>
									<p>Worked as Professor and Head of the department at Sarda Collage, Rajkot,
										Gujarat
										from 2003-2015 </p>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting
										industry. Lorem
										Ipsum has been the industry's standard dummy text ever since the 1500s,
										when
										an unknown printer took a galley of type and scrambled it to make a
										type
										specimen book. It has survived not only five centuries, but also the
										leap
										into electronic typesetting, remaining essentially unchanged.</p>
									<div class="section-title">Education</div>
									<ul>
										<li>B.A.,Gujarat University, Ahmedabad,India.</li>
										<li>M.A.,Gujarat University, Ahmedabad, India.</li>
										<li>P.H.D., Shaurashtra University, Rajkot</li>
									</ul>
									<div class="section-title">Experience</div>
									<ul>
										<li>One year experience as Jr. Professor from April-2009 to march-2010
											at B.
											J. Arts College, Ahmedabad.</li>
										<li>Three year experience as Jr. Professor at V.S. Arts &amp; Commerse
											Collage
											from April - 2008 to April - 2011.</li>
										<li>Lorem Ipsum is simply dummy text of the printing and typesetting
											industry.
										</li>
										<li>Lorem Ipsum is simply dummy text of the printing and typesetting
											industry.
										</li>
										<li>Lorem Ipsum is simply dummy text of the printing and typesetting
											industry.
										</li>
										<li>Lorem Ipsum is simply dummy text of the printing and typesetting
											industry.
										</li>
									</ul>
								</div>
								<div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
									<form enctype="multipart/form-data" class="needs-validation">
										<div class="card-header">
											<h4>Edit Profile</h4>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="form-group col-md-6 col-12">
													<label for="idadmin">User ID</label>
													<input class="form-control" type="text" readonly id="idadmin" name="idadmin" value="<?= $profil->idadmin ?>">
													<div class="invalid-feedback">
														Please fill in the first name
													</div>
												</div>
												<div class="form-group col-md-6 col-12">
													<label for="idadmin">Nama Admin</label>
													<input class="form-control" type="text" id="namaadmin" name="namaadmin" value="<?= $profil->namaadmin ?>">
													<div class="invalid-feedback">
														Please fill in the last name
													</div>
												</div>
												<div class="form-group col-md-6 col-12">
													<label for="alamat">Alamat</label>
													<input class="form-control" type="text" id="alamat" name="alamat" value="<?= $profil->alamat ?>">
													<div class="invalid-feedback">
														Please fill in the Alamat
													</div>
												</div>
												<div class="form-group col-md-6 col-12">
													<label for="tangallahir">Tanggal Lahir</label>
													<input class="form-control" type="date" id="tanggallahir" name="tanggallahir" value="<?= $profil->tanggallahir ?>">
													<div class="invalid-feedback">
														Please fill in the tangallahir
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6 col-12">
													<label>Email</label>
													<input class="form-control" type="email" id="email" name="email" value="<?= $profil->email ?>"></td>
													<div class="invalid-feedback">
														Please fill in the email
													</div>
												</div>
												<div class="form-group col-md-6 col-12">
													<label>Telepon</label>
													<input class="form-control" type="telepon" id="telepon" name="telepon" value="<?= $profil->telepon ?>"></td>
													<div class="invalid-feedback">
														Please fill in the telepon
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-12">
													<label>Agama</label>
													<select id="agama" name="agama" class="form-control">
														<option value="<?= $profil->agama ?>" selected disabled hidden>
															<?= $profil->agama ?></option>
														<option value="Islam">Islam</option>
														<option value="Kristen">Kristen</option>
														<option value="Katolik">Katolik</option>
														<option value="Hindu">Hindu</option>
														<option value="Buddha">Buddha</option>
														<option value="Lainnya">Lainnya</option>
													</select>
												</div>
											</div>
											<div class="row">
												<?php
												if ($profil->jekel = 'L') {
													$j = 'Laki-Laki';
												} else {
													$j = 'Perempuan';
												};
												?>
												<div class="form-group col-12">
													<label>Jenis Kelamin</label>
													<select id="jekel" name="jekel" class="form-control">
														<option value="<?= $profil->jekel ?>" selected disabled hidden><?= $j ?>
														</option>
														<option value="L">Laki-Laki</option>
														<option value="P">Perempuan</option>
													</select>
												</div>
											</div>
										</div>
										<div class="card-footer text-right">
											<button class="btn btn-danger" id="batal" style="margin-top:20px; margin-left:10px;">
												<span class="icon-floppy-disk"></span> Batal</button>
											<button class="btn btn-primary" id="simpan" style="margin-top:20px; ">
												<span class="icon-floppy-disk"></span> Simpan</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="settingSidebar">
		<a href="javascript:void(0)" class="settingPanelToggle"> <i class="fa fa-spin fa-cog"></i>
		</a>
		<div class="settingSidebar-body ps-container ps-theme-default">
			<div class=" fade show active">
				<div class="setting-panel-header">Setting Panel
				</div>
				<div class="p-15 border-bottom">
					<h6 class="font-medium m-b-10">Select Layout</h6>
					<div class="selectgroup layout-color w-50">
						<label class="selectgroup-item">
							<input type="radio" name="value" value="1" class="selectgroup-input-radio select-layout" checked>
							<span class="selectgroup-button">Light</span>
						</label>
						<label class="selectgroup-item">
							<input type="radio" name="value" value="2" class="selectgroup-input-radio select-layout">
							<span class="selectgroup-button">Dark</span>
						</label>
					</div>
				</div>
				<div class="p-15 border-bottom">
					<h6 class="font-medium m-b-10">Sidebar Color</h6>
					<div class="selectgroup selectgroup-pills sidebar-color">
						<label class="selectgroup-item">
							<input type="radio" name="icon-input" value="1" class="selectgroup-input select-sidebar">
							<span class="selectgroup-button selectgroup-button-icon" data-toggle="tooltip" data-original-title="Light Sidebar"><i class="fas fa-sun"></i></span>
						</label>
						<label class="selectgroup-item">
							<input type="radio" name="icon-input" value="2" class="selectgroup-input select-sidebar" checked>
							<span class="selectgroup-button selectgroup-button-icon" data-toggle="tooltip" data-original-title="Dark Sidebar"><i class="fas fa-moon"></i></span>
						</label>
					</div>
				</div>
				<div class="p-15 border-bottom">
					<h6 class="font-medium m-b-10">Color Theme</h6>
					<div class="theme-setting-options">
						<ul class="choose-theme list-unstyled mb-0">
							<li title="white" class="active">
								<div class="white"></div>
							</li>
							<li title="cyan">
								<div class="cyan"></div>
							</li>
							<li title="black">
								<div class="black"></div>
							</li>
							<li title="purple">
								<div class="purple"></div>
							</li>
							<li title="orange">
								<div class="orange"></div>
							</li>
							<li title="green">
								<div class="green"></div>
							</li>
							<li title="red">
								<div class="red"></div>
							</li>
						</ul>
					</div>
				</div>
				<div class="p-15 border-bottom">
					<div class="theme-setting-options">
						<label class="m-b-0">
							<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="mini_sidebar_setting">
							<span class="custom-switch-indicator"></span>
							<span class="control-label p-l-10">Mini Sidebar</span>
						</label>
					</div>
				</div>
				<div class="p-15 border-bottom">
					<div class="theme-setting-options">
						<label class="m-b-0">
							<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="sticky_header_setting">
							<span class="custom-switch-indicator"></span>
							<span class="control-label p-l-10">Sticky Header</span>
						</label>
					</div>
				</div>
				<div class="mt-4 mb-4 p-3 align-center rt-sidebar-last-ele">
					<a href="#" class="btn btn-icon icon-left btn-primary btn-restore-theme">
						<i class="fas fa-undo"></i> Restore Default
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('components/foot'); ?>
<?php $this->load->view('components/jsfoot2'); ?>
<script>
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#tampil').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}
	$("#image").change(function() {
		readURL(this);
	});

	$("#simpan").click(function() {
		alert('Profil disimpan!');
		simpanabsen();
	});

	$("#gantipassword").click(function() {
		window.location.href = "profil/gantipass";
	});

	function simpanabsen() {
		$.ajax({
			url: "profil/simpan",
			type: "POST",
			data: $("form").serialize(),
			dataType: "JSON",
			success: function(data) {
				window.location.reload();
				$("html, body").animate({
					scrollTop: 0
				}, "slow");
				$('#namafoto').load(document.URL + ' #namafoto');
			}
		})
	}
</script>
</body>

</html>