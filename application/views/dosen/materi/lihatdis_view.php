<?php $this->load->view('components/head2'); ?>
<base href="<?= base_url(); ?>">
<?php $this->load->view('components/navbardosen'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;"><span class="icon-briefcase"></span>
			Lihat Materi<small> Pembelajaran Online</small></h1>
		</div>
	<hr>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="quick-actions_homepage offset2">
				<ul class="quick-actions">
					<li class="bg_lo span3"> <a href="dosen/lihatup"> <i class="icon-book"></i>Materi PDF </a> </li>
					<li class="bg_lg span3"> <a href="dosen/lihatdis"> <i class="icon-th-list"></i>Materi Diskusi</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<hr>
	<div class="container-fluid">	
		<div class="ini"></div>
		<div class="row-fluid">
			<div class="span12">
			<input type="hidden" name="f" id="f" value="<?php echo $this->session->flashdata('berhasil');?>">
				<!-- Diskusi============================================================================== -->
				
				<div class="widget-box">
					<button class="btn btn-success pull-right" onclick="location.href='dosen/subbuatdiskusi'"
						style="margin:3px 5px;">
						<span class="icon-plus"></span> Tambah</button>
					<div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
						<h5>Materi Forum Diskusi</h5>
					</div>
					<div class="widget-content nopadding">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th style="width:50px;">ID Materi</th>
									<th style="width:50px;">Pertemuan</th>
									<th style="width:50px;">Semester</th>
									<th style="width:100px;">Program Studi</th>
									<th style="width:80px;">Matakuliah</th>
									<th style="width:auto;">Judul Materi</th>
									<th style="width:150px;">Tanggal</th>
									<th colspan="2">Action</th>
								</tr>
							</thead>
							<tbody id="tbldis">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>






<?php $this->load->view('components/foot'); ?>
<script src="assets/js/app/myfunction.js"></script>
<script src="assets/js/app/dosen/lihatdis.js"></script>
<script>
	$(document).ready(function () {
		alert("read");
		flash = $('#f').attr('value');
		if (flash == "Berhasil, Materi terhapus.") {
			var divMessage = "<div class='alert alert-success'>" +
				"<strong>Berhasil! </strong> <span>" + flash + "</span>" +
				"</div>";
			$(divMessage)
				.prependTo(".ini")
				.delay(2000)
				.slideUp("slow");
		}
	});
</script>
<?php $this->load->view('components/jsfoot2'); ?>

</body>

</html>