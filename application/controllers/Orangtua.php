<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orangtua extends CI_Controller
{
    public function index()
    {
        $this->load->view('orangtua/index');
    }

    public function profil()
    {
        $this->load->view('orangtua/profil');
    }

    public function presensi()
    {
        $this->load->view('orangtua/presensi');
    }

}
