<!DOCTYPE html>
<html lang="en">


<!-- profile.html  21 Nov 2019 03:49:30 GMT -->

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Profil | </title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/css/app.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/bootstrap-social/bootstrap-social.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/summernote/summernote-bs4.css">
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/css/style.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/css/components.css">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/css/custom.css">
    <link rel='shortcut icon' type='image/x-icon' href='<?= base_url('assets/') ?>/img/unu-1.png' />
</head>

<body>
    <!--   <div class="loader">
    
  </div> -->
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg">

            </div>
            <!-- Start Navigation Bar -->
            <div class="container">
                <header class="header_area" style="background-color: white !important; ">
                    <div class="main_menu">
                        <nav class="navbar navbar-expand-lg" style="width: 100%; margin-left: -18.5%">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <a class="navbar-brand logo_h" href="<?= base_url('welcome') ?>"><img src="<?= base_url('assets/') ?>img/unu-2.png" alt="" style="width: 225px; height: 55px; margin-left: 32px;"></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse offset" id="navbarSupportedContent" style="margin-right: 32px;">
                                <ul class="nav navbar-nav menu_nav ml-auto">
                                    <li class="nav-item ">
                                        <a class="nav-link" href="<?= base_url('user/profil') ?>" style="color: black;">Profil
                                        </a>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link" href="<?= base_url('user') ?>" style="color: black;">Beranda</a>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= base_url('user/kelas10') ?>" style="color: black;">Materi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= base_url('user/absensi') ?>" style="color: black;">Absensi</a>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= base_url('user/tugas') ?>" style="color: black;">Tugas</a>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= base_url('user/ujian') ?>" style="color: black;">Ujian</a>
                                    <li class=" nav-item " style="color: red;">
                                        <a class=" nav-link text-danger" href="<?= base_url('welcome/logout') ?>" style="color: black;">Keluar</a>
                                    </li>
                                    </li>
                                    </li>
                                    </li>
                                    </li>
                                </ul>
                            </div>
                    </div>
                    </nav>
            </div>
            </header>
            <!--       <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html"> <img alt="image" src="assets/img/logo.png" class="header-logo" /> <span
                class="logo-name">Otika</span>
            </a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Main</li>
            <li class="dropdown">
              <a href="index.html" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span></a>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="briefcase"></i><span>Widgets</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="widget-chart.html">Chart Widgets</a></li>
                <li><a class="nav-link" href="widget-data.html">Data Widgets</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="command"></i><span>Apps</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="chat.html">Chat</a></li>
                <li><a class="nav-link" href="portfolio.html">Portfolio</a></li>
                <li><a class="nav-link" href="blog.html">Blog</a></li>
                <li><a class="nav-link" href="calendar.html">Calendar</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="mail"></i><span>Email</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="email-inbox.html">Inbox</a></li>
                <li><a class="nav-link" href="email-compose.html">Compose</a></li>
                <li><a class="nav-link" href="email-read.html">read</a></li>
              </ul>
            </li>
            <li class="menu-header">UI Elements</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="copy"></i><span>Basic
                  Components</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="alert.html">Alert</a></li>
                <li><a class="nav-link" href="badge.html">Badge</a></li>
                <li><a class="nav-link" href="breadcrumb.html">Breadcrumb</a></li>
                <li><a class="nav-link" href="buttons.html">Buttons</a></li>
                <li><a class="nav-link" href="collapse.html">Collapse</a></li>
                <li><a class="nav-link" href="dropdown.html">Dropdown</a></li>
                <li><a class="nav-link" href="checkbox-and-radio.html">Checkbox &amp; Radios</a></li>
                <li><a class="nav-link" href="list-group.html">List Group</a></li>
                <li><a class="nav-link" href="media-object.html">Media Object</a></li>
                <li><a class="nav-link" href="navbar.html">Navbar</a></li>
                <li><a class="nav-link" href="pagination.html">Pagination</a></li>
                <li><a class="nav-link" href="popover.html">Popover</a></li>
                <li><a class="nav-link" href="progress.html">Progress</a></li>
                <li><a class="nav-link" href="tooltip.html">Tooltip</a></li>
                <li><a class="nav-link" href="flags.html">Flag</a></li>
                <li><a class="nav-link" href="typography.html">Typography</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="shopping-bag"></i><span>Advanced</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="avatar.html">Avatar</a></li>
                <li><a class="nav-link" href="card.html">Card</a></li>
                <li><a class="nav-link" href="modal.html">Modal</a></li>
                <li><a class="nav-link" href="sweet-alert.html">Sweet Alert</a></li>
                <li><a class="nav-link" href="toastr.html">Toastr</a></li>
                <li><a class="nav-link" href="empty-state.html">Empty State</a></li>
                <li><a class="nav-link" href="multiple-upload.html">Multiple Upload</a></li>
                <li><a class="nav-link" href="pricing.html">Pricing</a></li>
                <li><a class="nav-link" href="tabs.html">Tab</a></li>
              </ul>
            </li>
            <li><a class="nav-link" href="blank.html"><i data-feather="file"></i><span>Blank Page</span></a></li>
            <li class="menu-header">Otika</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="layout"></i><span>Forms</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="basic-form.html">Basic Form</a></li>
                <li><a class="nav-link" href="forms-advanced-form.html">Advanced Form</a></li>
                <li><a class="nav-link" href="forms-editor.html">Editor</a></li>
                <li><a class="nav-link" href="forms-validation.html">Validation</a></li>
                <li><a class="nav-link" href="form-wizard.html">Form Wizard</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="grid"></i><span>Tables</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="basic-table.html">Basic Tables</a></li>
                <li><a class="nav-link" href="advance-table.html">Advanced Table</a></li>
                <li><a class="nav-link" href="datatables.html">Datatable</a></li>
                <li><a class="nav-link" href="export-table.html">Export Table</a></li>
                <li><a class="nav-link" href="editable-table.html">Editable Table</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="pie-chart"></i><span>Charts</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="chart-amchart.html">amChart</a></li>
                <li><a class="nav-link" href="chart-apexchart.html">apexchart</a></li>
                <li><a class="nav-link" href="chart-echart.html">eChart</a></li>
                <li><a class="nav-link" href="chart-chartjs.html">Chartjs</a></li>
                <li><a class="nav-link" href="chart-sparkline.html">Sparkline</a></li>
                <li><a class="nav-link" href="chart-morris.html">Morris</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="feather"></i><span>Icons</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="icon-font-awesome.html">Font Awesome</a></li>
                <li><a class="nav-link" href="icon-material.html">Material Design</a></li>
                <li><a class="nav-link" href="icon-ionicons.html">Ion Icons</a></li>
                <li><a class="nav-link" href="icon-feather.html">Feather Icons</a></li>
                <li><a class="nav-link" href="icon-weather-icon.html">Weather Icon</a></li>
              </ul>
            </li>
            <li class="menu-header">Media</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="image"></i><span>Gallery</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="light-gallery.html">Light Gallery</a></li>
                <li><a href="gallery1.html">Gallery 2</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="flag"></i><span>Sliders</span></a>
              <ul class="dropdown-menu">
                <li><a href="carousel.html">Bootstrap Carousel.html</a></li>
                <li><a class="nav-link" href="owl-carousel.html">Owl Carousel</a></li>
              </ul>
            </li>
            <li><a class="nav-link" href="timeline.html"><i data-feather="sliders"></i><span>Timeline</span></a></li>
            <li class="menu-header">Maps</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="map"></i><span>Google
                  Maps</span></a>
              <ul class="dropdown-menu">
                <li><a href="gmaps-advanced-route.html">Advanced Route</a></li>
                <li><a href="gmaps-draggable-marker.html">Draggable Marker</a></li>
                <li><a href="gmaps-geocoding.html">Geocoding</a></li>
                <li><a href="gmaps-geolocation.html">Geolocation</a></li>
                <li><a href="gmaps-marker.html">Marker</a></li>
                <li><a href="gmaps-multiple-marker.html">Multiple Marker</a></li>
                <li><a href="gmaps-route.html">Route</a></li>
                <li><a href="gmaps-simple.html">Simple</a></li>
              </ul>
            </li>
            <li><a class="nav-link" href="vector-map.html"><i data-feather="map-pin"></i><span>Vector
                  Map</span></a></li>
            <li class="menu-header">Pages</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="user-check"></i><span>Auth</span></a>
              <ul class="dropdown-menu">
                <li><a href="auth-login.html">Login</a></li>
                <li><a href="auth-register.html">Register</a></li>
                <li><a href="auth-forgot-password.html">Forgot Password</a></li>
                <li><a href="auth-reset-password.html">Reset Password</a></li>
                <li><a href="subscribe.html">Subscribe</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="alert-triangle"></i><span>Errors</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="errors-503.html">503</a></li>
                <li><a class="nav-link" href="errors-403.html">403</a></li>
                <li><a class="nav-link" href="errors-404.html">404</a></li>
                <li><a class="nav-link" href="errors-500.html">500</a></li>
              </ul>
            </li>
            <li class="dropdown active">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="anchor"></i><span>Other
                  Pages</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="create-post.html">Create Post</a></li>
                <li><a class="nav-link" href="posts.html">Posts</a></li>
                <li class="active"><a class="nav-link" href="profile.html">Profile</a></li>
                <li><a class="nav-link" href="contact.html">Contact</a></li>
                <li><a class="nav-link" href="invoice.html">Invoice</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="chevrons-down"></i><span>Multilevel</span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Menu 1</a></li>
                <li class="dropdown">
                  <a href="#" class="has-dropdown">Menu 2</a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Child Menu 1</a></li>
                    <li class="dropdown">
                      <a href="#" class="has-dropdown">Child Menu 2</a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Child Menu 1</a></li>
                        <li><a href="#">Child Menu 2</a></li>
                      </ul>
                    </li>
                    <li><a href="#"> Child Menu 3</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </aside>
      </div> -->
            <!-- Main Content -->
            <div class="container">
                <section class="section">
                    <div class="section-body">
                        <div class="row mt-sm-4">
                            <div class="col-12 col-md-12 col-lg-4" style="margin-top: 89px;">
                                <div class="card author-box">
                                    <div class="card-body">
                                        <div class="author-box-center">
                                            <img alt="image" src="<?= base_url('assets/') ?>assets2/img/users/user-1.png" class="rounded-circle author-box-picture">
                                            <div class="clearfix"></div>
                                            <div class="author-box-name">
                                                <a href="#">Okta Brahvila Karim</a>
                                            </div>
                                            <div class="author-box-job">Informatika</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="author-box-description">
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur voluptatum alias molestias
                                                    minus quod dignissimos.
                                                </p>
                                            </div>
                                            <div class="mb-2 mt-3">
                                                <div class="text-small font-weight-bold">Prof. Purwo Santoso <br> (DPA)</div>
                                            </div>
                                            <a href="https://web.facebook.com/atko45/" class="btn btn-social-icon mr-1 btn-facebook">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                            <a href="#" class="btn btn-social-icon mr-1 btn-twitter">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                            <a href="#" class="btn btn-social-icon mr-1 btn-github">
                                                <i class="fab fa-github"></i>
                                            </a>
                                            <a href="https://www.instagram.com/atko450/" class="btn btn-social-icon mr-1 btn-instagram">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                            <div class="w-100 d-sm-none"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Detail </h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="py-4">
                                            <p class="clearfix">
                                                <span class="float-left">
                                                    Tanggal Lahir
                                                </span>
                                                <span class="float-right text-muted">
                                                    31 Oktober 1997
                                                </span>
                                            </p>
                                            <p class="clearfix">
                                                <span class="float-left">
                                                    No. HP
                                                </span>
                                                <span class="float-right text-muted">
                                                    +62 813 3363 7527
                                                </span>
                                            </p>
                                            <p class="clearfix">
                                                <span class="float-left">
                                                    E-mail
                                                </span>
                                                <span class="float-right text-muted">
                                                    oktakarim@student.unu-jogja.ac.id
                                                </span>
                                            </p>
                                            <p class="clearfix">
                                                <span class="float-left">
                                                    Facebook
                                                </span>
                                                <span class="float-right text-muted">
                                                    <a href="https://web.facebook.com/atko45/">atko45</a>
                                                </span>
                                            </p>
                                            <p class="clearfix">
                                                <span class="float-left">
                                                    Instagram
                                                </span>
                                                <span class="float-right text-muted">
                                                    <a href="https://www.instagram.com/atko450/">@atko450</a>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Skills</h4>
                                    </div>
                                    <div class="card-body">
                                        <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                                            <li class="media">
                                                <div class="media-body">
                                                    <div class="media-title">Java</div>
                                                </div>
                                                <div class="media-progressbar p-t-10">
                                                    <div class="progress" data-height="6">
                                                        <div class="progress-bar bg-primary" data-width="70%"></div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="media">
                                                <div class="media-body">
                                                    <div class="media-title">Web Design</div>
                                                </div>
                                                <div class="media-progressbar p-t-10">
                                                    <div class="progress" data-height="6">
                                                        <div class="progress-bar bg-warning" data-width="80%"></div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="media">
                                                <div class="media-body">
                                                    <div class="media-title">Photoshop</div>
                                                </div>
                                                <div class="media-progressbar p-t-10">
                                                    <div class="progress" data-height="6">
                                                        <div class="progress-bar bg-green" data-width="48%"></div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-lg-8" style="margin-top: 89px;">
                                <div class="card">
                                    <div class="padding-20">
                                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">Biodata</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Ubah Data</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content tab-bordered" id="myTab3Content">
                                            <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                                                <div class="row">
                                                    <div class="col-md-3 col-6 b-r">
                                                        <strong>Nama Lengkap</strong>
                                                        <br>
                                                        <p class="text-muted">Okta Brahvila karim</p>
                                                    </div>
                                                    <div class="col-md-3 col-6 b-r">
                                                        <strong>No. HP</strong>
                                                        <br>
                                                        <p class="text-muted">+62 813 3633 7527</p>
                                                    </div>
                                                    <div class="col-md-3 col-6 b-r">
                                                        <strong>Email</strong>
                                                        <br>
                                                        <p class="text-muted">oktakarim@student.unu-jogja.ac.id</p>
                                                    </div>
                                                    <div class="col-md-3 col-6">
                                                        <strong>Alamat</strong>
                                                        <br>
                                                        <p class="text-muted">Tegallayang X, Caturharjo, Pandak, Bantul</p>
                                                    </div>
                                                </div>
                                                <p class="m-t-30">Completed my graduation in Arts from the well known and
                                                    renowned institution
                                                    of India – SARDAR PATEL ARTS COLLEGE, BARODA in 2000-01, which was
                                                    affiliated
                                                    to M.S. University. I ranker in University exams from the same
                                                    university
                                                    from 1996-01.</p>
                                                <p>Worked as Professor and Head of the department at Sarda Collage, Rajkot,
                                                    Gujarat
                                                    from 2003-2015 </p>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry. Lorem
                                                    Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                    when
                                                    an unknown printer took a galley of type and scrambled it to make a
                                                    type
                                                    specimen book. It has survived not only five centuries, but also the
                                                    leap
                                                    into electronic typesetting, remaining essentially unchanged.</p>
                                                <div class="section-title">Pendidikan</div>
                                                <ul>
                                                    <li>B.A.,Gujarat University, Ahmedabad,India.</li>
                                                    <li>M.A.,Gujarat University, Ahmedabad, India.</li>
                                                    <li>P.H.D., Shaurashtra University, Rajkot</li>
                                                </ul>
                                                <div class="section-title">Prestasi</div>
                                                <ul>
                                                    <li>One year experience as Jr. Professor from April-2009 to march-2010
                                                        at B.
                                                        J. Arts College, Ahmedabad.</li>
                                                    <li>Three year experience as Jr. Professor at V.S. Arts &amp; Commerse
                                                        Collage
                                                        from April - 2008 to April - 2011.</li>
                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry.
                                                    </li>
                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry.
                                                    </li>
                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry.
                                                    </li>
                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                        industry.
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                                                <form method="post" class="needs-validation">
                                                    <div class="card-header">
                                                        <h4>Ubah Profil</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="form-group col-md-6 col-12">
                                                                <label>Nama Depan</label>
                                                                <input type="text" class="form-control" value="Okta">
                                                                <div class="invalid-feedback">
                                                                    Please fill in the first name
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6 col-12">
                                                                <label>Nama Belakang</label>
                                                                <input type="text" class="form-control" value="Brahvila Karim">
                                                                <div class="invalid-feedback">
                                                                    Please fill in the last name
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-7 col-12">
                                                                <label>Email</label>
                                                                <input type="email" class="form-control" value="oktakarim@student.unu-jogja.ac.id">
                                                                <div class="invalid-feedback">
                                                                    Please fill in the email
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-5 col-12">
                                                                <label>No. HP</label>
                                                                <input type="tel" class="form-control" value="">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-12">
                                                                <label>Tentang Kamu</label>
                                                                <textarea class="form-control summernote-simple">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur voluptatum alias molestias minus quod dignissimos.</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group mb-0 col-12">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" name="remember" class="custom-control-input" id="newsletter">
                                                                    <label class="custom-control-label" for="newsletter">Subscribe to newsletter</label>
                                                                    <div class="text-muted form-text">
                                                                        You will get new information about products, offers and promotions
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer text-right">
                                                        <button class="btn btn-primary">Save Changes</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <footer class="main-footer">
                <div class="footer-left">
                    <a href="templateshub.net">Templateshub</a></a>
                </div>
                <div class="footer-right">
                </div>
            </footer>
        </div>
    </div>
    <!-- General JS Scripts -->
    <script src="<?= base_url('assets/') ?>assets2/js/app.min.js"></script>
    <!-- JS Libraies -->
    <script src="<?= base_url('assets/') ?>assets2/bundles/summernote/summernote-bs4.js"></script>
    <!-- Page Specific JS File -->
    <!-- Template JS File -->
    <script src="<?= base_url('assets/') ?>assets2/js/scripts.js"></script>
    <!-- Custom JS File -->
    <script src="<?= base_url('assets/') ?>assets2/js/custom.js"></script>
</body>


<!-- profile.html  21 Nov 2019 03:49:32 GMT -->

</html>