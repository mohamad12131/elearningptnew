<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function index()
	{
		if ($this->session->userdata("islogin") == 'masuk') {
			redirect("home");
		} else if ($this->session->userdata("islogin") == 'masukmhs') {
			redirect('hmahasiswa/home');
		} else if ($this->session->userdata("islogin") == 'masukdsn') {
			redirect('dosen/masuk');
		} else {
			// $this->load->view('template/nav');
			// $this->load->view('template/index');
			// $this->load->view('template/footer');
			$this->load->view('login_view');
		}
	}

	public function home()
	{
		$this->load->view('template/nav');
		$this->load->view('template/index');
		$this->load->view('template/footer');
	}

	function ceklogin()
	{
		$userid = $this->input->post("userid");
		$password = do_hash($this->input->post("password"), "md5");
		// $this->session->set_userdata('ses_passdosen',$password);
		$statusadm = 'admin';
		$statusdsn = 'dosen';
		$cek_admin = $this->Login_model->ambiladmin($userid, $password, $statusadm);
		$cek_dosen = $this->Login_model->ambiladmin($userid, $password, $statusdsn);
		$cek_mahasiswa = $this->Login_model->ambilmahasiswa($userid, $password);
		if ($cek_admin->num_rows() == 1) { //jika login sebagai admin
			$data = $cek_admin->row_array();
			$this->session->set_userdata('islogin', 'masuk');
			$this->session->set_userdata('ses_id', $data['idadmin']);
			$this->session->set_userdata('ses_nama', $data['namaadmin']);
			redirect('home');
		} elseif ($cek_mahasiswa->num_rows() == 1) { //jika login sebagai mahasiswa
			$data = $cek_mahasiswa->row_array();
			$this->session->set_userdata('islogin', 'masukmhs');
			$this->session->set_userdata('ses_id', $data['nim']);
			$this->session->set_userdata('ses_nama', $data['namamhs']);
			$this->session->set_userdata('ses_prodi', $data['prodi']);
			$this->session->set_userdata('ses_semester', $data['semester']);
			$this->session->set_userdata('ses_kelas', $data['kelas']);
			redirect('hmahasiswa/home');
		} elseif ($cek_dosen->num_rows() == 1) { //jika login sebagai dosen
			$data = $cek_dosen->row_array();
			$this->session->set_userdata('ismasuk', 'masuk');
			$this->session->set_userdata('ses_id', $data['idadmin']);
			$this->session->set_userdata('ses_nama', $data['namaadmin']);
			redirect('dosen/masuk');
		} else {  // jika userid dan password tidak ditemukan atau salah
			echo $this->session->set_flashdata(
				"error-login",
				"User id atau Password Salah!"
			);
			redirect('login');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata("ses_id", "ses_nama", "islogin", "ismasuk");
		$this->session->sess_destroy();
		redirect("login");
	}


	private function login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$user = $this->db->get_where('siswa', ['email' => $email])->row_array();

		if ($user) {
			//user ada
			if ($user['is_active'] == 1) {
				//cek password
				if (password_verify($password, $user['password'])) {
					$data = [
						'email' => $user['email'],
					];

					$this->session->set_userdata($data);
					redirect(base_url('user'));
				} else {
					$this->session->set_flashdata('fail-pass', 'Gagal!');
					redirect(base_url('welcome'));
				}
			} else {
				$this->session->set_flashdata('fail-email', 'Gagal!');
				redirect(base_url('welcome'));
			}
		} else {
			$this->session->set_flashdata('fail-login', 'Gagal!');
			redirect(base_url('welcome'));
		}
	}

	public function admin()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', [
			'required' => 'Harap isi bidang email!',
			'valid_email' => 'Email tidak valid!',
		]);
		$this->form_validation->set_rules('password', 'Password', 'trim|required', [
			'required' => 'Harap isi bidang password!',
		]);
		if ($this->form_validation->run() == false) {
			$this->load->view('admin/login');
		} else {
			//validasi sukses
			$this->adminlogin();
		}
	}

	private function adminlogin()
	{
		$userid = $this->input->post('userid');
		$password = $this->input->post('password');

		$user = $this->db->get_where('admin', ['userid' => $userid])->row_array();

		if ($user) {
			//cek password
			if (password_verify($password, $user['password'])) {
				$data = [

					'userid' => $user['userid'],
					'nama' => $user['username'],

				];
				$this->session->set_userdata($data);
				redirect(base_url('admin'));
			} else {

				$this->session->set_flashdata('fail-pass', 'Gagal!');
				redirect(base_url('welcome/admin'));
			}
		} else {

			$this->session->set_flashdata('fail-login', 'Gagal!');
			redirect(base_url('welcome/admin'));
		}
	}

	public function tentang()
	{
		$this->load->view('template/nav2');
		$this->load->view('tentang2');
		$this->load->view('template/footer2');
	}

	public function pelajaran()
	{
		$this->load->view('template/nav2');
		$this->load->view('pelajaran2');
		$this->load->view('template/footer2');
	}

	public function kontak()
	{
		$this->load->view('template/nav2');
		$this->load->view('kontak2');
		$this->load->view('template/footer2');
	}

	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_where('siswa', ['email' => $email])->row_array();
		if ($user) {
			$user_token = $this->db->get_where('token', ['token => $token'])->row_array();
			if ($user_token) {
				if (time() - $user_token['date_created'] < (600 * 600 * 24)) {
					$this->db->set('is_active', 1);
					$this->db->where('email', $email);
					$this->db->update('siswa');

					$this->db->delete('token', ['email' => $email]);
					$this->session->set_flashdata('success-verify', 'Bserhasil!');
					redirect(base_url('welcome'));
				} else {
					$this->db->delete('siswa', ['email' => $email]);
					$this->db->delete('token', ['email' => $email]);

					$this->session->set_flashdata('fail-token-expired', 'gagal');
					redirect(base_url('welcome'));
				}
			} else {
				$this->session->set_flashdata('fail-token', 'gagal');
				redirect(base_url('welcome'));
			}
		} else {
			$this->session->set_flashdata('fail-verify', 'gagal');
			redirect(base_url('welcome'));
		}
	}

	// Guru
	public function guru()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', [
			'required' => 'Harap isi bidang email!',
			'valid_email' => 'Email tidak valid!',
		]);
		$this->form_validation->set_rules('password', 'Password', 'trim|required', [
			'required' => 'Harap isi bidang password!',
		]);
		if ($this->form_validation->run() == false) {
			$this->load->view('guru/login');
		} else {
			//validasi sukses
			$this->guru_login_process();
		}
	}

	private function guru_login_process()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$user = $this->db->get_where('guru', ['email' => $email])->row_array();

		if ($user) {
			//cek password
			if (password_verify($password, $user['password'])) {
				$data = [

					'email' => $user['email'],
					'nama_guru' => $user['nama_guru'],

				];
				$this->session->set_userdata($data);
				redirect(base_url('guru'));
			} else {

				$this->session->set_flashdata('fail-pass', 'Gagal!');
				redirect(base_url('welcome/guru'));
			}
		} else {

			$this->session->set_flashdata('fail-login', 'Gagal!');
			redirect(base_url('welcome/guru'));
		}
	}

	public function email()
	{
		$this->load->view('template/email-template');
	}
}
