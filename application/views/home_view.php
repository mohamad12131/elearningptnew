<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>

<!--main-container-part-->
<div class="main-content">
	<section class="section">
		<div class="row ">
			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="card">
					<div class="card-statistic-4">
						<div class="align-items-center justify-content-between">
							<div class="row ">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
									<div class="card-content">
										<h5 class="font-15">Mahasiswa</h5>
										<h2 class="mb-3 font-18">200</h2>
										<p class="mb-0"><span class="col-green">10%</span> Increase</p>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
									<div class="banner-img">
										<img src="<?= base_url('assets/') ?>assets2/img/banner/1.png" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="card">
					<div class="card-statistic-4">
						<div class="align-items-center justify-content-between">
							<div class="row ">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
									<div class="card-content">
										<h5 class="font-15"> Dosen</h5>
										<h2 class="mb-3 font-18">200</h2>
										<p class="mb-0"><span class="col-orange">09%</span> Decrease</p>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
									<div class="banner-img">
										<img src="<?= base_url('assets/') ?>assets2/img/banner/2.png" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="card">
					<div class="card-statistic-4">
						<div class="align-items-center justify-content-between">
							<div class="row ">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
									<div class="card-content">
										<h5 class="font-15">Kelas</h5>
										<h2 class="mb-3 font-18">128</h2>
										<p class="mb-0"><span class="col-green">18%</span>
											Increase</p>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
									<div class="banner-img">
										<img src="<?= base_url('assets/') ?>assets2/img/banner/3.png" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="card">
					<div class="card-statistic-4">
						<div class="align-items-center justify-content-between">
							<div class="row ">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
									<div class="card-content">
										<h5 class="font-15">Materi</h5>
										<h2 class="mb-3 font-18">100</h2>
										<p class="mb-0"><span class="col-green">42%</span> Increase</p>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
									<div class="banner-img">
										<img src="<?= base_url('assets/') ?>assets2/img/banner/4.png" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Action boxes-->
		<hr>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="widget-box">
					<div class="widget-title"> <span class="icon"> <i class="icon-bullhorn"></i> </span>
						<h5>Pengumuman</h5>
					</div>
					<div class="widget-content">
						<table id="tblpeng" width='100%'>

						</table>
					</div>
				</div>
			</div>
			<form name="form1" id="form1">
				<div class="control-group">
					<div class="controls">
						<textarea class="span12" rows="4" name="isi" id="isi" placeholder="Ketikan soal..."></textarea>
						<span name="isi"></span>
					</div>
				</div>
				<input type="hidden" name="isihide" id="isihide">
				<!-- <input class="btn btn-danger pull-right" style=" margin-left:5px;" type="reset" value="Batal" /> -->
				<input class="btn btn-success pull-right" type="submit" value="Kirim" id="kirim" />
			</form>
		</div>
	</section>
</div>

<!--end-main-container-part-->

<?php $this->load->view('components/foot'); ?>
<script src="assets/js/app/home.js"></script>

<script src="assets/ckeditor/ckeditor.js"></script>
<script src="assets/ckfinder/ckfinder.js"></script>
<script>
	CKEDITOR.replace('isi');
	CKFinder.setupCKEditor();
</script>
</body>

</html>