<!-- Menggunakan Source Code Okta -->

<body>
	<div class="loader"></div>
	<div id="app">
		<div class="main-wrapper main-wrapper-1">
			<div class="navbar-bg"></div>
			<nav class="navbar navbar-expand-lg main-navbar sticky">
				<div class="form-inline mr-auto">
					<ul class="navbar-nav mr-3">
						<li>
							<a href="#" data-toggle="sidebar" class="nav-link nav-link-lg collapse-btn">
								<i data-feather="align-justify"></i>
							</a>
						</li>
					</ul>
				</div>
				<ul class="navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img alt="image" src="<?= base_url("assets/img/fotomahasiswa/" . $this->session->userdata("ses_id") . ".jpg") ?>" class="user-img-radious-style">
							<span class="d-sm-none d-lg-inline-block"></span>
						</a>
						<div class="dropdown-menu dropdown-menu-right pullDown">
							<div class="dropdown-title">
								Hai,
								<?= $this->session->userdata("ses_nama"); ?>
								<?php if ($this->session->userdata("islogin") == 'masukdsn') { ?>

							</div>
							<a href="<?= base_url('profil/dosen') ?>" class="dropdown-item has-icon">
								<i class="far fa-user"></i> Profil
							</a>
							<a href="<?= base_url('profil/gantipass') ?>" class="dropdown-item has-icon">
								<i class="far fa-user"></i> Ganti Password
							</a>
							<div class="dropdown-divider"></div>
							<a href="<?= base_url('welcome/logout') ?>" class="dropdown-item has-icon text-danger">
								<i class="fas fa-sign-out-alt"></i>
								Keluar
							</a>
						</div>
					</li>
				<?php } ?>
				</ul>
			</nav>
			<!--sidebar-menu-->
			<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="home"> <img alt="image" src="<?php echo base_url('assets2/img/unu-2.png'); ?>" class="header-logo" /> <span class="logo-name">Otika</span>
						</a>
					</div>
					<ul class="sidebar-menu">
						<li class="menu-header">Main</li>
						<li class="dropdown">
							<a href="home" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span></a>
						</li>
						<li class="menu-header">Manajemen Kuliah</li>
						<li class="dropdown">
							<a href="dosen/soal" class="nav-link"><i data-feather="briefcase"></i><span>Manajemen Pertanyaan</span></a>
						</li>
						<li class="dropdown">
							<a href="dosen/diskusi" class="nav-link"><i data-feather="monitor"></i><span>Forum Diskusi</span></a>
						</li>
						<li class="dropdown">
							<a href="dosen/materi" class="nav-link"><i data-feather="monitor"></i><span>Management Materi</span></a>
						</li>
						<li class="dropdown">
							<a href="dosen/nilai" class="nav-link"><i data-feather="monitor"></i><span>Penilaian</span></a>
						</li>
						<li class="dropdown">
							<a href="laporan/absensilaporandosen" class="nav-link"><i data-feather="monitor"></i><span>Laporan Nilai</span></a>
						</li>
						<li class="dropdown">
							<a href="laporan/nilailaporandosen" class="nav-link"><i data-feather="monitor"></i><span>Laporan Absensi</span></a>
						</li>

					</ul>
				</aside>
			</div>
</body>