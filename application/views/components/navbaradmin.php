<body>
	<div class="loader"></div>
	<div id="app">
		<div class="main-wrapper main-wrapper-1">
			<div class="navbar-bg"></div>
			<nav class="navbar navbar-expand-lg main-navbar sticky">
				<div class="form-inline mr-auto">
					<ul class="navbar-nav mr-3">
						<li>
							<a href="#" data-toggle="sidebar" class="nav-link nav-link-lg collapse-btn">
								<i data-feather="align-justify"></i>
							</a>
						</li>
					</ul>
				</div>
				<ul class="navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img alt="image" src="<?= base_url("assets/img/fotomahasiswa/" . $this->session->userdata("ses_id") . ".jpg") ?>" class="user-img-radious-style">
							<span class="d-sm-none d-lg-inline-block"></span>
						</a>
						<div class="dropdown-menu dropdown-menu-right pullDown">
							<div class="dropdown-title">
								Hai,
								<?= $this->session->userdata("ses_nama"); ?>
								<?php if ($this->session->userdata("islogin") == 'masuk') { ?>

							</div>
							<a href="<?= base_url('profil') ?>" class="dropdown-item has-icon">
								<i class="far fa-user"></i> Profil
							</a>
							<a href="<?= base_url('profil/gntpassword') ?>" class="dropdown-item has-icon">
								<i class="far fa-user"></i> Ganti Password
							</a>
							<div class="dropdown-divider"></div>
							<a href="<?= base_url('welcome/logout') ?>" class="dropdown-item has-icon text-danger">
								<i class="fas fa-sign-out-alt"></i>
								Keluar
							</a>
						</div>
					</li>
				<?php } ?>
				</ul>
			</nav>
			<!--sidebar-menu-->
			<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="home"> <img alt="image" src="<?php echo base_url('assets2/img/unu-2.png'); ?>" class="header-logo" /> <span class="logo-name">Otika</span>
						</a>
					</div>
					<ul class="sidebar-menu">
						<li class="menu-header">Main</li>
						<li class="dropdown active">
							<a href="home" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span></a>
						</li>
						<li class="menu-header">Manajemen Pengguna</li>
						<li class="dropdown">
							<a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="briefcase"></i><span>Mahasiswa</span></a>
							<ul class="dropdown-menu">
								<li><a class="nav-link" href="admin/data_mhs">Data Mahasiswa</a></li>
								<li><a class="nav-link" href="admin/add_mahasiswa">Tambah Data Siswa</a>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="briefcase"></i><span>Dosen</span></a>
							<ul class="dropdown-menu">
								<li><a class="nav-link" href="<?= base_url('admin/data_guru') ?>">Data Dosen</a></li>
								<li><a class="nav-link" href="mahasiswaadd">Tambah Data Dosen</a>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="briefcase"></i><span>Rektor</span></a>
							<ul class="dropdown-menu">
								<li><a class="nav-link" href="mahasiswa">Data Data </a></li>
								<li><a class="nav-link" href="mahasiswaadd">Tambah Data Siswa</a>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="briefcase"></i><span>Orangtua</span></a>
							<ul class="dropdown-menu">
								<li><a class="nav-link" href="mahasiswa">Data Mahasiswa</a></li>
								<li><a class="nav-link" href="mahasiswaadd">Tambah Data Siswa</a>
							</ul>
						</li>
						<li class="menu-header">Management Kuliah</li>
						<li class="dropdown">
							<a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="command"></i><span>Management Kuliah</span></a>
							<ul class="dropdown-menu">
								<li><a class="nav-link" href="matakuliah">Mata Kuliah</a></li>
								<li><a class="nav-link" href="materi">Data materi</a></li>
								<li><a class="nav-link" href="soal">Data soal</a></li>
								<li><a class="nav-link" href="diskusi">Data Forum diskusi</a></li>
							</ul>
						</li>
						<li class="menu-header">Management Admin</li>
						<li class="dropdown">
							<a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="command"></i><span>Management Admin</span></a>
							<ul class="dropdown-menu">
								<li><a class="nav-link" href="admin">Data Admin</a></li>
								<li><a class="nav-link" href="admin/add_admin">Tambah Data Admin</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="command"></i><span>Management Laporan</span></a>
							<ul class="dropdown-menu">
								<li><a href="laporan/mahasiswalaporan">Laporan Mahasiswa</a></li>
								<li><a class="nav-link" href="laporan/absensilaporan">Laporan Absensi</a></li>
								<li><a class="nav-link" href="laporan/matakuliahlaporan">Laporan Matakuliah</a></li>
								<li><a class="nav-link" href="laporan/nilailaporan">Laporan Nilai</a></li>
							</ul>
						</li>
					</ul>
				</aside>
			</div>
</body>
<div class="settingSidebar">
	<a href="javascript:void(0)" class="settingPanelToggle"> <i class="fa fa-spin fa-cog"></i>
	</a>
	<div class="settingSidebar-body ps-container ps-theme-default">
		<div class=" fade show active">
			<div class="setting-panel-header">Setting Panel
			</div>
			<div class="p-15 border-bottom">
				<h6 class="font-medium m-b-10">Select Layout</h6>
				<div class="selectgroup layout-color w-50">
					<label class="selectgroup-item">
						<input type="radio" name="value" value="1" class="selectgroup-input-radio select-layout" checked>
						<span class="selectgroup-button">Light</span>
					</label>
					<label class="selectgroup-item">
						<input type="radio" name="value" value="2" class="selectgroup-input-radio select-layout">
						<span class="selectgroup-button">Dark</span>
					</label>
				</div>
			</div>
			<div class="p-15 border-bottom">
				<h6 class="font-medium m-b-10">Sidebar Color</h6>
				<div class="selectgroup selectgroup-pills sidebar-color">
					<label class="selectgroup-item">
						<input type="radio" name="icon-input" value="1" class="selectgroup-input select-sidebar">
						<span class="selectgroup-button selectgroup-button-icon" data-toggle="tooltip" data-original-title="Light Sidebar"><i class="fas fa-sun"></i></span>
					</label>
					<label class="selectgroup-item">
						<input type="radio" name="icon-input" value="2" class="selectgroup-input select-sidebar" checked>
						<span class="selectgroup-button selectgroup-button-icon" data-toggle="tooltip" data-original-title="Dark Sidebar"><i class="fas fa-moon"></i></span>
					</label>
				</div>
			</div>
			<div class="p-15 border-bottom">
				<h6 class="font-medium m-b-10">Color Theme</h6>
				<div class="theme-setting-options">
					<ul class="choose-theme list-unstyled mb-0">
						<li title="white" class="active">
							<div class="white"></div>
						</li>
						<li title="cyan">
							<div class="cyan"></div>
						</li>
						<li title="black">
							<div class="black"></div>
						</li>
						<li title="purple">
							<div class="purple"></div>
						</li>
						<li title="orange">
							<div class="orange"></div>
						</li>
						<li title="green">
							<div class="green"></div>
						</li>
						<li title="red">
							<div class="red"></div>
						</li>
					</ul>
				</div>
			</div>
			<div class="p-15 border-bottom">
				<div class="theme-setting-options">
					<label class="m-b-0">
						<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="mini_sidebar_setting">
						<span class="custom-switch-indicator"></span>
						<span class="control-label p-l-10">Mini Sidebar</span>
					</label>
				</div>
			</div>
			<div class="p-15 border-bottom">
				<div class="theme-setting-options">
					<label class="m-b-0">
						<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="sticky_header_setting">
						<span class="custom-switch-indicator"></span>
						<span class="control-label p-l-10">Sticky Header</span>
					</label>
				</div>
			</div>
			<div class="mt-4 mb-4 p-3 align-center rt-sidebar-last-ele">
				<a href="#" class="btn btn-icon icon-left btn-primary btn-restore-theme">
					<i class="fas fa-undo"></i> Restore Default
				</a>
			</div>
		</div>
	</div>
</div>