<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>

<!--main-container-part-->
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card-body">
					<h2 class="card-title">Materi Diskusi</h2>
					<hr>
					<a href="<?= base_url('lihatup') ?>" class="btn btn-outline-primary">
						Materi PDF</a>
					<a href="<?= base_url('lihatdis') ?>" class="btn btn-outline-primary">
						Materi Diskusi</a>
				</div>
			</div>
			<button class="btn btn-success" onclick="location.href='subuploadmateri'" style="float: right;">Tambah Materi</button>
			<div class="row">
				<div class="col-12">
					<input type="hidden" name="f" id="f" value="<?php echo $this->session->flashdata('berhasil'); ?>">
					<div class="accordion" id="collapsefilter">
						<div class="accordion-group" style="width:400px;">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#collapsefilter" href="#tampilfilter">
									<strong>Filter Data <span class="icon icon-chevron-down"></span></strong>
								</a>
							</div>
							<div id="tampilfilter" class="accordion-body collapse in">
								<div class="accordion-inner">
									<form class="form-inline">
										<table>
											<tr>
												<td><label for="matakuliahfil">Matakuliah</label></td>
												<td>
													<select class="span12" name="matakuliahfil" id="matakuliahfil">
														<option value="" disabled selected>Pilih</option>
														<?php
														$namamk = $this->db->query("select * from tblmatakuliah");
														foreach ($namamk->result() as $row) {
															echo "<option value='" . $row->kodemk . "'>" . $row->namamk . " | " . $row->prodi . " - " . $row->semester . "</option>";
														}

														?>
													</select>
												</td>
											</tr>
											<tr>
												<td>&nbsp</td>
												<td><button type="submit" id="filter" class="btn">Filter</button></td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-striped table-hover" style="width:100%;float:right;">
											<thead>
												<tr>
													<th style="width:30px;">Pertemuan</th>
													<th style="width:30px;">Semester</th>
													<th style="width:150px;">Program Studi</th>
													<th style="width:80px;">Matakuliah</th>
													<th style="width:auto;">Judul Materi</th>
													<th style="width:150px;">Tanggal</th>
													<th colspan="2">Action</th>
												</tr>
											</thead>
											<tbody id="tbldis">
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>






				<?php $this->load->view('components/foot'); ?>
				<script src="<?= base_url('assets/') ?>js/app/myfunction.js"></script>
				<script src="<?= base_url('assets/') ?>js/app/lihatdis.js"></script>

				<!-- <script>
	$(document).ready(function () {
		alert("read");
		flash = $('#f').attr('value');
		if (flash == "Berhasil, Materi terhapus.") {
			var divMessage = "<div class='alert alert-success'>" +
				"<strong>Berhasil! </strong> <span>" + flash + "</span>" +
				"</div>";
			$(divMessage)
				.prependTo(".ini")
				.delay(2000)
				.slideUp("slow");
		}
	});
</script> -->
				<script>
					$(document).ready(function() {
						flash = $('#f').attr('value');
						if (flash == "Materi ter-upload.") {
							var divMessage = "<div class='alert alert-success'>" +
								"<strong>Berhasil! </strong> <span>" + flash + "</span>" +
								"</div>";
							$(divMessage)
								.prependTo(".ini")
								.delay(2000)
								.slideUp("slow");
						}
						$('#matakuliah').on('change', function() {
							// alert('asd');
							prodi = $(this).find(':selected').data('prodi');
							semester = $(this).find(':selected').data('sem');
							console.log(prodi, semester);
							$("#prodi").val(prodi);
							$("#semester").val(semester);
						});
					});
				</script>
				<?php $this->load->view('components/jsfoot2'); ?>

				</body>

				</html>