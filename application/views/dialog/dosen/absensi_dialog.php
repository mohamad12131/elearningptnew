<?php $this->load->view('components/head2'); ?>
<base href="<?= base_url(); ?>">
<?php $this->load->view('components/navbardosen'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;"><span class="icon-briefcase"></span>
				Laporan Absensi <small>Universitas Nahdlatul Ulama Yogyakarta</small></h1>
		</div>
		<hr>
		<div class="container-fluid offset4">
			<form action="<?= $action ?>" method="GET" target="blank">
				<div class="control-group">
					<label class="control-label" for="prodi">Matakuliah</label>
					<div class="controls">
						<select name="kodemk" id="kodemk" class="span4">
							<option value="" disabled selected>Pilih</option>
							<?php
							$namamk = $this->db->query("select * from tblmatakuliah");
							foreach ($namamk->result() as $row) {
								echo "<option value='" . $row->kodemk . "'>" . $row->namamk . " - " . $row->prodi . " - " . $row->semester . "</option>";
							}
							?>
						</select>
						<?php echo form_error('prodi'); ?>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="kelas">Kelas</label>
					<div class="controls">
						<select id="kelas" name="kelas" class="form-control">
							<option disabled selected>Pilih</option>
							<option value="P1">P1</option>
							<option value="P2">P2</option>
							<option value="M1">M1</option>
						</select>
						<?php echo form_error('kelas'); ?>
					</div>
				</div>
				<!-- <input type="hidden" name="prodi" id="prodi">
				<input type="hidden" name="semester" id="semester"> -->
				<input type="submit" id="submit" class="btn btn-success" value="Cetak Laporan">
		</div>
		</form>
</div>
</div>

<?php $this->load->view('components/foot'); ?>
<script>
	$(document).ready(function() {
		$('#matakuliah').on('change', function() {
			alert('asd');
			prodi = $(this).find(':selected').data('prodi');
			semester = $(this).find(':selected').data('sem');
			console.log(prodi, semester);
			$("#prodi").val(prodi);
			$("#semester").val(semester);
		});
	});
</script>
<?php $this->load->view('components/jsfoot2'); ?>
</body>

</html>