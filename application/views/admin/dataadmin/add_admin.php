<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="">
            <div class="card" style="width:100%;">
                <div class="card-body">
                    <h2 class="card-title" style="color: black;">Tambah Data Pengguna</h2>
                    <hr>
                    <p class="card-text"> After I ran into Helen at a restaurant, I realized she was just office pretty drop-dead date put in in a deck for our standup today. Who's responsible for the ask for this request? who's responsible for the ask for this request? but moving the goalposts gain traction.
                    </p>
                    <a href="#detail" class="btn btn-success">Saya paham dan
                        ingin melanjutkan ⭢</a>
                </div>
            </div>
        </div>

        <div id="detail" class="card card-success">
            <div class="col-md-12 text-center">
                <p class="registration-title font-weight-bold display-4 mt-4" style="color:black; font-size: 50px;">
                    Pendaftaran Guru</p>
                <p style="line-height:-30px;margin-top:-20px;">Silahkan isi data data yang diperlukan
                    dibawah </p>
                <hr>
            </div>

            <div class="card-body">
                <form method="POST" action="<?= base_url('admin/add_admin') ?>">
                    <div id="" class="form-group">
                        <label for="nip">Nomor Induk Pegawai</label>
                        <input id="nip" type="text" class="form-control" name="nip">
                        <?= form_error('nip', '<small class="text-d nanger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div id="" class="form-group">
                        <label for="telepon">Telepon</label>
                        <input id="telepon" type="text" class="form-control" name="telepon">
                        <?= form_error('telepon', '<small class="text-danger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div id="" class="form-group">
                        <label for="alamat">Alamat</label>
                        <input id="alamat" type="text" class="form-control" name="alamat">
                        <?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div id="" class="form-group">
                        <label for="tanggallahir">tanggal lahir</label>
                        <input id="tanggallahir" type="date" class="form-control" name="tanggallahir">
                        <?= form_error('tanggallahir', '<small class="text-danger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div id="" class="form-group">
                        <label for="agama">Agama</label>
                        <select id="agama" name="agama" class="form-control">
                            <option value="" disabled selected>Pilih</option>
                            <option value="Islam">Islam</option>
                            <option value="Kristen">Kristen</option>
                            <option value="Katolik">Katolik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Buddha">Buddha</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                        <span name="agama"></span>
                        <?= form_error('agama', '<small class="text-danger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="email" class="form-control" name="email">
                        <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jekel">Jenis Kelamin</label>
                        <select id="jekel" name="jekel" class="form-control">
                            <option value="" disabled selected>Pilih</option>
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                        <span name="jekel"></span>
                        <?= form_error('namaadmin', '<small class="text-danger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="namaadmin">Nama Lengkap</label>
                        <input id="namaadmin" type="text" class="form-control" name="namaadmin">
                        <?= form_error('namaadmin', '<small class="text-danger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status">status</label>
                        <select id="status" name="status" class="form-control">
                            <option value="" disabled selected>Pilih</option>
                            <option value="admin">Admin</option>
                            <option value="dosen">Dosen</option>
                            <option value="rektor">Rektor</option>
                            <option value="ortu">Orangtua</option>
                        </select>
                        <span name="status"></span>
                        <?= form_error('status', '<small class="text-danger">', '</small>'); ?>
                        <div class="invalid-feedback">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label for="password" class="d-block">Password</label>
                            <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password">
                            <?= form_error('password', '<small class="text-danger">', '</small>'); ?>
                            <div id="pwindicator" class="pwindicator">
                                <div class="bar"></div>
                                <div class="label"></div>
                            </div>
                        </div>
                        <div class="form-group col-6">
                            <label for="password2" class="d-block">Konfirmasi Password</label>
                            <input id="password2" type="password" class="form-control" name="password2">
                            <?= form_error('password2', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="agree" class="custom-control-input" id="agree">
                            <label class="custom-control-label" for="agree">Saya Mengerti dan ingin
                                melajutkan.</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg btn-block">
                            Daftar ⭢
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </section>
</div>
</div>
</div>
<!-- End Main Content -->
<?php $this->load->view('components/jsfoot2'); ?>