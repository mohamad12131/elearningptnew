<!--================Home Banner Area =================-->
<section class="home_banner_area" style="margin-top: -245px; padding-bottom: 2.4px">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background="">
        </div>
        <div class="container">
            <div class="banner_content text-center" style="margin-top: 245px;">
                <h3 data-aos="fade-up" data-aos-duration="1600">Belajar Dimana Saja & Kapan Saja <br /> Mudah Dengan Learn-Tara</h3>
                <p data-aos="fade-up" data-aos-duration="1900">Dengan Learn-Tara kemudahan kegiatan belajar mengajar dapat terpenuhi. Dosen dan mahasiswa dapat
                    belajar meski banyak halangan atau rintangan. Nikmati Pembelajaran terstruktur dan efektif menggunakan Learn-Tara serta kemudahan belajar dengan menggunakan aplikasi kami. </p>
                <a data-aos="fade-up" data-aos-duration="2000" class="main_btn" href="<?= base_url('user/registration') ?>#registration">Bergabung Sekarang</span></a>
                <hr>
            </div>
        </div>
    </div>
</section>
<!-- ================End Home Banner Area ================= -->

<!--================Finance Area =================-->
<section class="finance_area">
    <div class="container">
        <div class="finance_inner row">
            <div class="col-lg-3 col-sm-6">
                <div class="finance_item">
                    <div data-aos="fade-right" data-aos-duration="1600" class="media">
                        <div class="d-flex">
                            <i class="lnr lnr-users"></i>
                        </div>
                        <div class="media-body">
                            <h5>Absensi, materi dan perkuliahan online</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="finance_item">
                    <div data-aos="fade-right" data-aos-duration="1800" class="media">
                        <div class="d-flex">
                            <i class="lnr lnr-file-empty"></i>
                        </div>
                        <div class="media-body">
                            <h5>Upload tugas dan materi</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-right" data-aos-duration="2000" class="col-lg-3 col-sm-6">
                <div class="finance_item">
                    <div class="media">
                        <div class="d-flex">
                            <i class="lnr lnr-camera-video"></i>
                        </div>
                        <div class="media-body">
                            <h5>Pembelajaran melalui video</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-right" data-aos-duration="2200" class="col-lg-3 col-sm-6">
                <div class="finance_item">
                    <div class="media">
                        <div class="d-flex">
                            <i class="lnr lnr-tag"></i>
                        </div>
                        <div class="media-body">
                            <h5>Perkuliahan praktis, simpel dan fleksibel</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Finance Area =================-->

<!--================ Illustrations Area =================-->
<section class="lernify-for-indonesia p_20">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <img data-aos="fade-up" data-aos-duration="1800" src="<?= base_url('assets2/') ?>img/illustrations/index-study.svg" alt="" srcset="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 mx-auto">
                <div class="main_title">
                    <h2 data-aos="fade-up" data-aos-duration="2000">Learn-Tara Dibuat Untuk Meningkatkan Kualitas Pembelajaran Di Indonesia</h2>
                    <p data-aos="fade-up" data-aos-duration="2200">Sistem Informasi Pembelajaran berbasis Website. Dibuat guna membantu Universitas dalam memudahkan kegiatan belajar mengajar. Sangat efektif untuk penugasan dan berbagi materi antar program studi.</p>
                    <a href="https://github.com/syauqi/Learn-Tara"><button data-aos="fade-up" data-aos-duration="2400" class="bubbly-button main_btn" style="margin-top: 32px; border: none;">Download Learn-Tara</span></button></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Illustrations Area =================-->

<!--================Courses Area =================-->
<section class="team_area p_20">
    <div class="container">
        <div class="main_title">
            <h2 data-aos="fade-up" data-aos-duration="1600">Pembelajaran Yang Tersedia di Learn-Tara</h2>
            <p data-aos="fade-up" data-aos-duration="1800">Kamu dapat belajar materi pembelajaran dari fakultas lain di kampusmu. Lihat pembelajaran program studi lain agar dapat terintegritas dengan program studi kamu. Dengan materi yang dibagika oleh setiap prodi kamu dapat belajar berbagai macam ilmu pengetahuan.</p>
        </div>
        <div class="row courses_inner">
            <div class="col-lg-9">
                <div class="grid_inner">
                    <div class="grid_item wd55">
                        <div class="courses_item" data-aos="fade-right" data-aos-duration="1800">
                            <img src="<?= base_url('assets2/') ?>img/courses/fti-1.png" alt="" style="border-radius: 13px;" width="458" height="220">
                            <div class="hover_text">
                                <a href="javaScript:void(0);">
                                    <h4>Fakultas Teknologi Informasi</h4>
                                </a>
                                <ul class="list">
                                    <li><a href="#"><i class="lnr lnr-user"></i>Kharis Yugi Fatkhurrohman, S.T</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="grid_item wd44">
                        <div class="courses_item" data-aos="fade-down" data-aos-duration="1800">
                            <img src="<?= base_url('assets2/') ?>img/courses/fip-1.png" alt="" style="border-radius: 13px;" width="360" height="220">
                            <div class="hover_text">
                                <a href="javaScript:void(0);">
                                    <h4>Fakultas Ilmu Pendidikan</h4>
                                </a>
                                <ul class="list">
                                    <li><a href="#"><i class="lnr lnr-user"></i>Mohamad Hatami, S.Kom</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="grid_item wd44">
                        <div class="courses_item" data-aos="fade-right" data-aos-duration="1800">
                            <img src="<?= base_url('assets2/') ?>img/courses/fih-1.png" alt="" style="border-radius: 13px;" width="360" height="220">
                            <div class="hover_text">
                                <a href="javaScript:void(0);">
                                    <h4>Fakultas Industri Halal</h4>
                                </a>
                                <ul class="list">
                                    <li><a href="#"><i class="lnr lnr-user"></i> Okta Brahvila Karim, S.Farm</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="grid_item wd55">
                        <div class="courses_item" data-aos="fade-up" data-aos-duration="1800">
                            <img src="<?= base_url('assets2/') ?>img/courses/fe-1.png" alt="" style="border-radius: 13px; " width="458" height="220">
                            <div class="hover_text">
                                <a href="javaScript:void(0);">
                                    <h4>Fakultas Ekonomi</h4>
                                </a>
                                <ul class="list">
                                    <li><a href="#"><i class="lnr lnr-user"></i> Isra Rustam, S.E</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="course_item" data-aos="fade-left" data-aos-duration="1800">
                    <img src="<?= base_url('assets2/') ?>img/courses/di-1.png" alt="" style="border-radius: 13px;" width="262" height="470">
                    <div class="hover_text">
                        <a href="javaScript:void(0);">
                            <h4>Fakultas Dirosah Islamiyah</h4>
                        </a>
                        <ul class="list">
                            <li><a href="#"><i class="lnr lnr-user"></i>Ahmad Semsudin, S.Ag</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Courses Area =================-->

<!--================Team Area =================-->
<section class="courses_area p_40">
    <div class="container">
        <div class="main_title">
            <h2 data-aos="fade-up" data-aos-duration="1800">Learn-Tara didukung oleh:</h2>
        </div>
        <section class="testimonials_area p_20">
            <div class="container">
                <div class="testi_slider owl-carousel">
                    <div class="item">
                        <div class="testi_item" style="border-radius: 13px;">
                            <img src="<?= base_url('assets2/') ?>img/testimonials/rektor.png" alt="" height="130" width="130">
                            <h4>Prof. Drs. Purwo Santoso, M.A., Ph.D.</h4>
                            <ul class="list">
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                            </ul>
                            <p>Merciful revaluation burying love ultimate value inexpedient ubermensch. Holiest madness victorious morality hope endless christian madness. Love dead fearful transvaluation marvelous. Oneself right ideal abstract endless faith deceptions zarathustra grandeur law ubermensch free. Abstract chaos snare play truth ultimate good self. God overcome sexuality pious abstract good decieve revaluation aversion good. Virtues chaos overcome society holiest truth.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testi_item" style="border-radius: 13px;">
                            <img src="<?= base_url('assets2/') ?>img/testimonials/unu-3.png" alt="" height="160" width="160">
                            <h4>Universitas Nahdlatul Ulama Yogyakarta</h4>
                            <ul class="list">
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                            </ul>
                            <p>Merciful revaluation burying love ultimate value inexpedient ubermensch. Holiest madness victorious morality hope endless christian madness. Love dead fearful transvaluation marvelous. Oneself right ideal abstract endless faith deceptions zarathustra grandeur law ubermensch free. Abstract chaos snare play truth ultimate good self. God overcome sexuality pious abstract good decieve revaluation aversion good. Virtues chaos overcome society holiest truth.</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testi_item" style="border-radius: 13px;">
                            <img src="<?= base_url('assets2/') ?>img/testimonials/inf-1.png" alt="" height="130" width="130">
                            <h4>Advanced Informatics</h4>
                            <ul class="list">
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                            </ul>
                            <p>Merciful revaluation burying love ultimate value inexpedient ubermensch. Holiest madness victorious morality hope endless christian madness. Love dead fearful transvaluation marvelous. Oneself right ideal abstract endless faith deceptions zarathustra grandeur law ubermensch free. Abstract chaos snare play truth ultimate good self. God overcome sexuality pious abstract good decieve revaluation aversion good. Virtues chaos overcome society holiest truth.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<!--================End Team Area =================-->

<!--================Impress Area =================-->
<section class="impress_area p_120" width="1728" height="704" style="margin-top: 32px;">
    <div class="container">
        <div class="impress_inner text-center">
            <h2 data-aos="fade-up" data-aos-duration="1800">Masuk Sebagai Dosen</h2>
            <p data-aos="fade-up" data-aos-duration="2000">Merciful revaluation burying love ultimate value inexpedient ubermensch. Holiest madness victorious morality hope endless christian madness. Love dead fearful transvaluation marvelous. Oneself right ideal abstract endless faith deceptions zarathustra grandeur law ubermensch free. Abstract chaos snare play truth ultimate good self. God overcome sexuality pious abstract good decieve revaluation aversion good. Virtues chaos overcome society holiest truth.
            </p>
            <a data-aos="fade-up" data-aos-duration="2200" class="main_btn" href="<?= base_url('welcome/guru') ?>">Masuk Sebagai DOSEN</span></a>
        </div>
    </div>
</section>
<!--================End Impress Area =================-->