<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card">
					<div class="card-body">
						<h2 class="card-title">Buat Soal dan Upload Materi </h2>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="padding-20">
								<ul class="nav nav-tabs" id="myTab2" role="tablist">
									<li class="nav-item" style=" display: block; margin-left: auto; margin-right: 0;">
										<a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">Soal Essai</a>
									</li>
									<li class="nav-item" style=" display: block; margin-left: 0; margin-right: auto;">
										<a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Soal Pilihan Ganda</a>
									</li>
								</ul>
								<div class="tab-content tab-bordered" id="myTab3Content">
									<div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
										<form method="POST" action="soalpilgan/simpan" name="form1" enctype="multipart/form-data" class="needs-validation">
											<input type="hidden" name="tanggal" id="tanggal" value="<?= date('Y-m-d H:i:s'); ?>">
											<div class="row">
												<div class="card-body">
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Mata Kuliah</label>
														<div class="col-sm-12 col-md-7">
															<select class="form-control selectric" name="matakuliah" id="matakuliah">
																<option disabled selected>Pilih</option>
																<?php
																$namamk = $this->db->query("select * from tblmatakuliah");
																foreach ($namamk->result() as $row) {
																	echo "<option value='" . $row->kodemk . "'>" . $row->namamk . " | " . $row->prodi . " - " . $row->semester . "</option>";
																}
																foreach ($namamk->result() as $row) {
																	echo "<input type='hidden' id='" . $row->kodemk . "' name='" . $row->kodemk . "' value='" . $row->semester . "'>";
																}

																?>
															</select>
															<span name="matakuliah"></span>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="idsoal">Pertemuan Ke</label>
														<div class="col-sm-12 col-md-7">
															<input type="text" name="idsoal" id="idsoal" readonly value="-" class=" form-control">
														</div>
														<span name="idsoal"></span>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="idsoal">No Soal</label>
														<div class="col-sm-12 col-md-7">
															<input type="text" id="noesai" name="noesai" readonly class=" form-control">
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Soal</label>
														<div class="col-sm-12 col-md-7">
															<textarea class="summernote-simple" rows="4" name="isiesai" id="isiesai"></textarea>
															<span name="isiesai"></span>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jawaban</label>
														<div class="col-sm-12 col-md-7">
															<textarea class="summernote-simple" rows="4" name="jawaban" id="jawaban"></textarea>
															<span name="jawaban"></span>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
														<div class="col-sm-6 col-md-7">
															<button class="btn btn-info pull-right" type="submit" id="kirimesai">Kirim Ke Mahasiswa</button>
															<button class="btn btn-primary pull-right" type="submit" id="tambahesai" style="margin-right:10px;">Tambah Essai</button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
										<form method="POST" action="soalpilgan/simpan" name="form1" id="form1" enctype="multipart/form-data" class="needs-validation">
											<input type="hidden" name="tanggal" id="tanggal" value="<?= date('Y-m-d H:i:s'); ?>">
											<div class="row">
												<div class="card-body">
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Mata Kuliah</label>
														<div class="col-sm-12 col-md-7">
															<select class="form-control selectric" name="matakuliah" id="matakuliah">
																<option value="" disabled selected>Pilih</option>
																<?php
																$namamk = $this->db->query("select * from tblmatakuliah");
																foreach ($namamk->result() as $row) {
																	echo "<option value='" . $row->kodemk . "'>" . $row->namamk . " - " . $row->prodi . " - " . $row->semester . "</option>";
																}
																?>
															</select>
															<span name="matakuliah"></span>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="idsoal">Pertemuan Ke</label>
														<div class="col-sm-12 col-md-7">
															<input type="text" name="idsoalpilgan" id="idsoalpilgan" readonly value="-" class=" form-control">
														</div>
														<span name="idsoalpilgan"></span>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="idsoal">No Soal</label>
														<div class="col-sm-12 col-md-7">
															<input type="text" id="nopilgan" name="nopilgan" readonly class=" form-control">
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Isi Soal</label>
														<div class="col-sm-12 col-md-7">
															<textarea class="summernote-simple" rows="6" name="isipilgan" id="isipilgan"></textarea>
															<span name="isipilgan"></span>

														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pilihan A</label>
														<div class="col-sm-12 col-md-7">
															<textarea class="summernote-simple" rows="2" name="pila" id="pila"></textarea>
															<span name="pila"></span>

														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pilihan B</label>
														<div class="col-sm-12 col-md-7">
															<textarea class="summernote-simple" rows="2" name="pilb" id="pilb"></textarea>
															<span name="pilb"></span>

														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pilihan C</label>
														<div class="col-sm-12 col-md-7">
															<textarea class="summernote-simple" rows="2" name="pilc" id="pilc"></textarea>
															<span name="pilc"></span>

														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pilihan D</label>
														<div class="col-sm-12 col-md-7">
															<textarea class="summernote-simple" rows="2" name="pild" id="pild"></textarea>
															<span name="pild"></span>

														</div>
													</div>
													<div class="form-group row mb-4">
														<label for="jawabanpilgan" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Program Studi</label>
														<div class="col-sm-12 col-md-7">
															<select class="form-control selectric" name="jawabanpilgan" id="jawabanpilgan">
																<option value="" disabled selected>Pilih</option>
																<option value="A">A</option>
																<option value="B">B</option>
																<option value="C">C</option>
																<option value="D">D</option>
															</select>
															<span name="jawabanpilgan"></span>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
														<div class="col-sm-6 col-md-7">
															<button class="btn btn-primary" type="submit" id="simpan">Tambah Materi</button>
															<button type="reset" onclick="window.location.reload()" class="btn btn-danger">reset</button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>

	<?php $this->load->view('components/foot'); ?>
	<?php $this->load->view('components/jsfoot2'); ?>
	<script src="assets/js/app/myfunction.js"></script>
	<script src="assets/js/app/soalesai.js"></script>
	<!-- <script src="assets/js/app/soalpilgan.js"></script> -->
	<script src="assets/ckeditor/ckeditor.js"></script>
	<script src="assets/ckfinder/ckfinder.js"></script>
	<script>
		CKEDITOR.replace('isipilgan', {
			width: 870,
			height: 100
		});
		CKEDITOR.replace('pila', {
			width: 870,
			height: 100
		});
		CKEDITOR.replace('pilb', {
			width: 870,
			height: 100
		});
		CKEDITOR.replace('pilc', {
			width: 870,
			height: 100
		});
		CKEDITOR.replace('pild', {
			width: 870,
			height: 100
		});
		CKFinder.setupCKEditor();
	</script>

	<script>
		$(document).ready(function() {
			flash = $('#f').attr('value');
			if (flash == "Materi ter-upload.") {
				var divMessage = "<div class='alert alert-success'>" +
					"<strong>Berhasil! </strong> <span>" + flash + "</span>" +
					"</div>";
				$(divMessage)
					.prependTo(".ini")
					.delay(2000)
					.slideUp("slow");
			}
			$('#matakuliah').on('change', function() {
				// alert('asd');
				prodi = $(this).find(':selected').data('prodi');
				semester = $(this).find(':selected').data('sem');
				console.log(prodi, semester);
				$("#prodi").val(prodi);
				$("#semester").val(semester);
			});
		});
	</script>
	<script>
		var editor = CKEDITOR.replace('file');
		CKFinder.setupCKEditor(editor);
	</script>

	</body>

	</html>