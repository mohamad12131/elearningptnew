<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//validasi jika user belum login
		if ($this->session->userdata('islogin') != 'masuk') {
			redirect('login');
		}
		$this->load->model("admin_model");
		$this->load->helper("security");
	}

	public function index()
	{
		$this->load->model('admin_model');
		$data['user'] = $this->db->get_where('tbladmin', ['idadmin' =>
		$this->session->userdata('idadmin')])->row_array();

		$data['user'] = $this->admin_model->ambiladmin()->result();
		$this->load->view('admin/dataadmin/data_admin', $data);
	}

	public function data_mhs()
	{
		$this->load->model('mahasiswa_model');
		$data['user'] = $this->db->get_where(
			'tblmahasiswa',
			['nim' => $this->session->userdata('nim')]
		)->row_array();
		$data['user'] = $this->mahasiswa_model->ambilMahasiswa()->result();
		$this->load->view('mahasiswa_view', $data);
	}

	public function add_admin()
	{
		$this->form_validation->set_rules('idadmin', 'idadmin', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom NIP.',
			'min_length' => 'NIP terlalu pendek.',
		]);

		$this->form_validation->set_rules('namaadmin', 'namaadmin', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom nama.',
			'min_length' => 'nama terlalu pendek.',
		]);

		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[guru.email]', [
			'is_unique' => 'Email ini telah digunakan!',
			'required' => 'Harap isi kolom email.',
			'valid_email' => 'Masukan email yang valid.',
		]);
		$this->form_validation->set_rules('taggallahir', 'taggallahir', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom tanggal lahir.',
			'min_length' => 'tangal lahir terlalu pendek.',
		]);
		$this->form_validation->set_rules('agama', 'agama', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom nama.',
			'min_length' => 'nama terlalu pendek.',
		]);
		$this->form_validation->set_rules('jekel', 'jekel', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom jenis kelamin.',
			'min_length' => 'nama terlalu pendek.',
		]);
		$this->form_validation->set_rules('status', 'status', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom jenis kelamin.',
			'min_length' => 'nama terlalu pendek.',
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|matches[password2]', [
			'required' => 'Harap isi kolom Password.',
			'matches' => 'Password tidak sama!',
			'min_length' => 'Password terlalu pendek',
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password]', [
			'matches' => 'Password tidak sama!',
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('admin/dataadmin/add_admin');
		} else {
			$data = [
				'idadmin' => htmlspecialchars($this->input->post('idadmin', true)),
				'namaadmin' => htmlspecialchars($this->input->post('namaadmin', true)),
				'alamat' => htmlspecialchars($this->input->post('alamat', true)),
				'email' => htmlspecialchars($this->input->post('email', true)),
				'tanggallahir' => htmlspecialchars($this->input->post('tanggallahir', true)),
				'agama' => htmlspecialchars($this->input->post('agama', true)),
				'jekel' => htmlspecialchars($this->input->post('jekel', true)),
				'status' => htmlspecialchars($this->input->post('status', true)),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			];

			$this->db->insert('tbladmin', $data);

			$this->session->set_flashdata('success-reg', 'Berhasil!');
			redirect(base_url('admin/dataadmin/data_admin'));
		}
	}

	public function gantipass()
	{
		$this->load->view("gantipassword_view");
	}

	public function ganti()
	{
		$rules = array(
			array(
				"field" => "passwordlama",
				"label" => "Password Lama",
				"rules" => "required"
			),
			array(
				"field" => "passwordbaru",
				"label" => "Password Baru",
				"rules" => "required|min_length[6]"
			),
			array(
				"field" => "konfirm",
				"label" => "Konfirm Password Baru",
				"rules" => "required|min_length[6]|matches[password-baru]"
			)
		);
		$this->form_validation->set_rules($rules);
		$this->form_validation->set_error_delimiters("<span class='help-block'>", "</span>");

		if ($this->form_validation->run()) {
			$userid = $this->session->userdata("ses_id");
			$password_lama = md5($this->input->post("passwordlama"), "md5");
			$password_baru = md5($this->input->post("passwordbaru"), "md5");

			if ($this->profil_model->cekLogin($userid, $password_lama)->num_rows() > 0) {
				$data = array(
					"idadmin" => $userid,
					"password" => $password_baru
				);

				$this->profil_model->updateUser($userid, $data);
				redirect("login/logout");
			} else {
				redirect("admin/gantipass");
			}
		} else {
			$this->load->view("profil_view");
		}
	}

	public function data()
	{
		echo json_encode($this->admin_model
			->ambiladmin()->result());
	}

	public function baca($id = null)
	{
		echo json_encode($this->admin_model
			->getadmin($id));
	}

	public function hapus($id)
	{
		echo json_encode(array(
			"status" => $this->admin_model->deleteadmin($id)
		));
	}

	public function reset($userid)
	{
		$data = array(
			"idadmin" => $userid,
			"password" => do_hash($userid, "md5")
		);

		echo json_encode(array(
			"status" => ($this->admin_model->updateadmin($userid, $data) > 0)
		));
	}

	public function simpan($mode)
	{
		if ($this->_validate($mode)) {
			$data = array(
				"idadmin" => $this->input->post("idadmin"),
				"namaadmin" => $this->input->post("namaadmin"),
				"alamat" => $this->input->post("alamat"),
				"email" => $this->input->post("email"),
				"tanggallahir" => $this->input->post("tanggallahir"),
				"agama" => $this->input->post("agama"),
				"jekel" => $this->input->post("jekel"),
				"telepon" => $this->input->post("telepon"),
				"status" => $this->input->post("status"),
				"password" => do_hash("123456", "md5")
			);

			if ($mode == "add") {
				$status = $this->admin_model->createadmin($data);
			} elseif ($mode == "edit") {
				$status = $this->admin_model
					->updateadmin($this->input->post("idadmin"), $data);
			}
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array(
				"status" => FALSE,
				"message" => array(
					"idadmin" => form_error("idadmin"),
					"namaadmin" => form_error("namaadmin"),
					"alamat" => form_error("alamat"),
					"email" => form_error("email"),
					"tanggallahir" => form_error("tanggallahir"),
					"agama" => form_error("agama"),
					"jekel" => form_error("jekel"),
					"telepon" => form_error("telepon"),
					"status" => form_error("status")
				)
			));
		}
	}

	private function _validate($mode)
	{
		$rules = array(
			array(
				"field" => "idadmin",
				"label" => "idadmin",
				"rules" => "required"
			),
			array(
				"field" => "namaadmin",
				"label" => "Nama",
				"rules" => "required"
			),
			array(
				"field" => "alamat",
				"label" => "Alamat",
				"rules" => "required"
			),
			array(
				"field" => "email",
				"label" => "Email",
				"rules" => "required"
			),
			array(
				"field" => "tanggallahir",
				"label" => "Tanggal Lahir",
				"rules" => "required"
			),
			array(
				"field" => "agama",
				"label" => "Agama",
				"rules" => "required"
			),
			array(
				"field" => "jekel",
				"label" => "Jenis Kelamin",
				"rules" => "required"
			),
			array(
				"field" => "telepon",
				"label" => "Telepon",
				"rules" => "required|max_length[25]"
			),
			array(
				"field" => "status",
				"label" => "status",
				"rules" => "required"
			)
		);

		$this->form_validation->set_rules($rules);
		$this->form_validation
			->set_error_delimiters("<span class='help-block alert alert-error nopadding'>", "</span>");

		return $this->form_validation->run();
	}

	public function simpanval($mode)
	{
		if ($this->_validate($mode)) {
			$data = array(
				"idadmin" => $this->input->post("idadmin"),
				"namaadmin" => $this->input->post("namaadmin"),
				"alamat" => $this->input->post("alamat"),
				"email" => $this->input->post("email"),
				"tanggallahir" => $this->input->post("tanggallahir"),
				"agama" => $this->input->post("agama"),
				"jekel" => $this->input->post("jekel"),
				"telepon" => $this->input->post("telepon"),
				"status" => $this->input->post("status"),
				"password" => do_hash("123456", "md5")
			);
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array(
				"status" => FALSE,
				"message" => array(
					"idadmin" => form_error("idadmin"),
					"namaadmin" => form_error("namaadmin"),
					"alamat" => form_error("alamat"),
					"email" => form_error("email"),
					"tanggallahir" => form_error("tanggallahir"),
					"agama" => form_error("agama"),
					"jekel" => form_error("jekel"),
					"telepon" => form_error("telepon"),
					"status" => form_error("status")
				)
			));
		}
	}

	function uploadedit()
	{
		$filename = $this->input->post("idadmin1");
		unlink("./assets/img/fotomahasiswa/" . $filename . ".jpg");
		$config = array(
			'upload_path' => './assets/img/fotomahasiswa',
			'allowed_types' => "gif|jpg|png|jpeg",
			'file_name' => $filename
		);

		$this->load->library('upload', $config);
		if ($this->upload->do_upload()) {
			$file_data = $this->upload->data();
			$data['nim'] = $filename;
			$data['file'] = $file_data['file_name'];
			// $this->mahasiswa_model->save_image($data);
		}
	}

	function upload()
	{
		$filename = $this->input->post("idadmin1");
		$config = array(
			'upload_path' => './assets/img/fotomahasiswa',
			'allowed_types' => "gif|jpg|png|jpeg",
			'file_name' => $filename
		);

		$this->load->library('upload', $config);
		if ($this->upload->do_upload()) {
			$file_data = $this->upload->data();
			$data['nim'] = $filename;
			$data['file'] = $file_data['file_name'];
			$this->mahasiswa_model->save_image($data);
		}
	}

	// dosen
	public function data_guru()
	{
		$this->load->model('m_guru');
		$data['user'] = $this->db->get_where('guru', ['email' =>
		$this->session->userdata('email')])->row_array();

		$data['user'] = $this->m_guru->tampil_data()->result();
		$this->load->view('admin/guru/data_guru', $data);
	}

	public function detail_guru($nip)
	{
		$this->load->model('m_guru');
		$where = array('nip' => $nip);
		$detail = $this->m_guru->detail_guru($nip);
		$data['detail'] = $detail;
		$this->load->view('admin/guru/detail_guru', $data);
	}

	public function detail_admin($idadmin)
	{
		$this->load->model('admin_model');
		$where = array('idadmin' => $idadmin);
		$detail = $this->admin_model->getadmin($idadmin);
		$data['detail'] = $detail;
		$this->load->view('admin/dataadmin/detail_admin', $data);
	}
	public function update_guru($nip)
	{
		$this->load->model('m_guru');
		$where = array('nip' => $nip);
		$data['user'] = $this->m_guru->update_guru($where, 'guru')->result();
		$this->load->view('admin/guru/update_guru', $data);
	}
	public function update_admin($idadmin)
	{
		$this->load->model('admin_model');
		$where = array('idadmin' => $idadmin);
		$data['user'] = $this->admin_model->updateadmin($where, 'tbladmin')->result();
		$this->load->view('admin/dataadmin/update_admin', $data);
	}
	public function guru_edit()
	{
		$this->load->model('m_guru');
		$nip = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');

		$data = array(
			'nip' => $nip,
			'nama_guru' => $nama,
			'email' => $email,

		);

		$where = array(
			'nip' => $nip,
		);

		$this->m_guru->update_data($where, $data, 'guru');
		$this->session->set_flashdata('success-edit', 'berhasil');
		redirect('admin/guru/data_guru');
	}
	public function add_guru()
	{
		$this->form_validation->set_rules('nip', 'Nip', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom NIP.',
			'min_length' => 'NIP terlalu pendek.',
		]);

		$this->form_validation->set_rules('nama', 'Nama', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom nAMA.',
			'min_length' => 'Nama terlalu pendek.',
		]);

		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[guru.email]', [
			'is_unique' => 'Email ini telah digunakan!',
			'required' => 'Harap isi kolom email.',
			'valid_email' => 'Masukan email yang valid.',
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|matches[password2]', [
			'required' => 'Harap isi kolom Password.',
			'matches' => 'Password tidak sama!',
			'min_length' => 'Password terlalu pendek',
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password]', [
			'matches' => 'Password tidak sama!',
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('dosenregistration');
		} else {
			$data = [
				'nip' => htmlspecialchars($this->input->post('nip', true)),
				'email' => htmlspecialchars($this->input->post('email', true)),
				'nama_guru' => htmlspecialchars($this->input->post('nama', true)),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			];

			$this->db->insert('guru', $data);

			$this->session->set_flashdata('success-reg', 'Berhasil!');
			redirect(base_url('admin/guru/data_guru'));
		}
	}
	public function delete_guru($nip)
	{
		$this->load->model('m_guru');
		$where = array('nip' => $nip);
		$this->m_guru->delete_guru($where, 'guru');
		$this->session->set_flashdata('user-delete', 'berhasil');
		redirect('admin/guru/data_guru');
	}

	// mahasisawa
	public function add_mhs()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim|min_length[4]', [
			'required' => 'Harap isi kolom username.',
			'min_length' => 'Nama terlalu pendek.',
		]);
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[siswa.email]', [
			'is_unique' => 'Email ini telah digunakan!',
			'required' => 'Harap isi kolom email.',
			'valid_email' => 'Masukan email yang valid.',
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|matches[retype_password]', [
			'required' => 'Harap isi kolom Password.',
			'matches' => 'Password tidak sama!',
			'min_length' => 'Password terlalu pendek',
		]);
		$this->form_validation->set_rules('retype_password', 'Password', 'required|trim|matches[password]', [
			'matches' => 'Password tidak sama!',
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('mhsregistration');
		} else {
			$email = $this->input->post('email', true);
			$data = [
				'nama' => htmlspecialchars($this->input->post('nama', true)),
				'email' => htmlspecialchars($email),
				'image' => 'default.jpg',
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'is_active' => 1,
				'date_created' => time(),
			];

			//siapkan token

			// $token = base64_encode(random_bytes(32));
			// $user_token = [
			//     'email' => $email,
			//     'token' => $token,
			//     'date_created' => time(),
			// ];

			$this->db->insert('siswa', $data);
			// $this->db->insert('token', $user_token);

			// $this->_sendEmail($token, 'verify');

			$this->session->set_flashdata('success-reg', 'Berhasil!');
			redirect(base_url('home'));
		}
	}

	private function _sendEmail($token, $type)
	{
		$config = [
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => 'ini email disini',
			'smtp_pass' => 'Isi Password gmail disini',
			'smtp_port' => 465,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n",
		];

		$this->email->initialize($config);

		$data = array(
			'name' => 'syauqi',
			'link' => ' ' . base_url() . 'welcome/verify?email=' . $this->input->post('email') . '& token' . urlencode($token) . '"',
		);

		$this->email->from('LearnifyEducations@gmail.com', 'Learnify');
		$this->email->to($this->input->post('email'));

		if ($type == 'verify') {
			$link =
				$this->email->subject('Verifikasi Akun');
			$body = $this->load->view('template/email-template.php', $data, true);
			$this->email->message($body);
		} else {
		}

		if ($this->email->send()) {
			return true;
		} else {
			echo $this->email->print_debugger();
			die();
		}
	}
}
