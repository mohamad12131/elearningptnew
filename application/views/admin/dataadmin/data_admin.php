<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>

<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card-body">
					<h2 class="card-title">Manajemen Data Admin </h2>
					<hr>
					<a href="<?= base_url('admin/add_mhs') ?>" class="btn btn-outline-primary">Tambah
						Data Admin ⭢</a>
				</div>
			</div>
			<!-- modal -->
			<div class="modal hide fade" tabindex="-1" role="dialog" id="form-admin" data-backdrop="false">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header">
							<h3>Form <span id="mode"></span> Admin</h3>
						</div>
						<div class="modal-body">
							<form class="form-horizontal" action="">
								<div class="control-group">
									<label class="control-label" for="idadmin">ID Admin</label>
									<div class="controls">
										<input type="number" id="idadmin" name="idadmin" class="form-control">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="namaadmin">Nama</label>
									<div class="controls">
										<input type="text" id="namaadmin" name="namaadmin" class="form-control">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="alamat">Alamat</label>
									<div class="controls">
										<input type="text" id="alamat" name="alamat" class="form-control">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="email">E-mail</label>
									<div class="controls">
										<input type="text" id="email" name="email" class="form-control">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="tanggallahir">Tangal Lahir</label>
									<div class="controls">
										<input type="date" id="tanggallahir" name="tanggallahir" class="form-control">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="agama">Agama</label>
									<div class="controls">
										<select id="agama" name="agama" class="form-control">
											<option value="" disabled selected>Pilih</option>
											<option value="Islam">Islam</option>
											<option value="Kristen">Kristen</option>
											<option value="Katolik">Katolik</option>
											<option value="Hindu">Hindu</option>
											<option value="Buddha">Buddha</option>
											<option value="Lainnya">Lainnya</option>
										</select>
										<span name="agama"></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="jekel">Jenis Kelamin</label>
									<div class="controls">
										<select id="jekel" name="jekel" class="form-control">
											<option value="" disabled selected>Pilih</option>
											<option value="L">Laki-Laki</option>
											<option value="P">Perempuan</option>
										</select>
										<span name="jekel"></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="telepon">Telepon</label>
									<div class="controls">
										<input type="text" id="telepon" name="telepon" class="form-control">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="status">Status</label>
									<div class="controls">
										<select id="status" name="status" class="form-control">
											<option value="" disabled selected>Pilih</option>
											<option value="admin">Admin</option>
											<option value="dosen">Dosen</option>
											<option value="rektor">Rektor</option>
											<option value="ortu">Orangtua</option>
										</select>
										<span name="status"></span>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button class="btn btn-success" id="simpandata">
								<span class="icon-floppy-disk"></span> Simpan</button>
							<button class="btn btn-warning" id="simpan">
								<span class="icon-floppy-disk" data-backdrop="false"></span> Selanjutnya</button>
							<button class="btn btn-danger" data-dismiss="modal">
								<span class="icon-remove"></span> Batal</button>
						</div>
					</div>
				</div>
			</div> <!-- ./modal -->

			<!-- modal gambar-->
			<div class="modal hide fade" tabindex="-1" role="dialog" id="form-gambar" data-backdrop="false">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header">
							<h3>Form <span id="mode"></span> Mahasiswa</h3>
						</div>
						<!-- action="mahasiswa/upload" method="POST" -->
						<div class="modal-body">
							<form name="form2" enctype="multipart/form-data">
								<input type="hidden" name="idadmin1" id="idadmin1">
								<div class="form-group">
									<label for="title">Foto</label>
									<input type="file" name="userfile" id="image" onchange="readURL(this)" />
								</div>
								<div class="form-group">
									<div id="previewLoad" style='margin-left: 0px; display: none'>
										<img src='<?php echo base_url(); ?>assets/img/loading.gif' />
									</div>
									<div class="image_preview">

									</div>
								</div>
								<input class="btn btn-danger pull-right" style="margin-top:20px; margin-left:5px;" type="submit" value="Batal" data-dismiss="modal" />
								<input class="btn btn-success pull-right" style="margin-top:20px;" type="submit" value="Simpan" id="simpangbr" />
							</form>
						</div>
						<!-- <div class="modal-footer">
						<button class="btn btn-success" type="submit">
							<span class="icon-floppy-disk"></span> Simpan</button>
						<button class="btn btn-danger" data-dismiss="modal">
							<span class="icon-remove"></span> Batal</button>
					</div> -->
					</div>
				</div>
			</div> <!-- ./modal -->

			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover" id="save-stage" style="width:100%;">
									<thead>
										<tr>
											<th>ID Admin</th>
											<th>Nama</th>
											<th>Alamat</th>
											<th>E-mail</th>
											<th>Tgl Lahir</th>
											<th>Agama</th>
											<th>Jekel</th>
											<th>Telepon</th>
											<th>Status</th>
											<th colspan="3">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php

										foreach ($user as $u) {
										?>
											<tr>

												<th scope="row">
													<?php echo $u->idadmin ?>
												</th>

												<td>
													<?php echo $u->namaadmin ?>
												</td>

												<td>
													<?php echo $u->alamat ?>
												</td>

												<td>
													<?php echo $u->email ?>
												</td>

												<td>
													<?php echo $u->tanggallahir ?>
												</td>
												<td>
													<?php echo $u->agama ?>
												</td>
												<td>
													<?php echo $u->jekel ?>
												</td>
												<td>
													<?php echo $u->telepon ?>
												</td>
												<td>
													<div class="badge badge-success">
														<?php echo $u->status ?>
													</div>
												</td>
												<td>
													<a href="<?php echo site_url('admin/detail_admin/' . $u->idadmin); ?>" class="btn btn-success">Detail</a>
												</td>
												<td>
													<a href="<?php echo site_url('admin/update_siswa/' . $u->idadmin); ?>" class="btn btn-info">Update</a>
												</td>

												<td>
													<a href="<?php echo site_url('admin/delete_siswa/' . $u->idadmin); ?>" class="btn btn-danger remove">Delete</a>
												</td>

											</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('components/foot'); ?>
<?php $this->load->view('components/jsfoot2'); ?>
<!-- 
			<script src="assets/js/app/myfunction.js"></script>
			<script src="assets/js/app/admin.js"></script> -->
<script>
	function readURL(input) {
		$('#previewLoad').show();
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('.image_preview').html('<img src="' + e.target.result + '" alt="' + reader.name +
					'" class="img-thumbnail" width="100" height="75"/>');
			}

			reader.readAsDataURL(input.files[0]);
			$('#previewLoad').hide();
		}
	}
</script>
</body>

</html>