<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{
    public function index()
    {
        $this->load->view('hmahasiswa/home_view');
    }

    public function profil()
    {
        $this->load->view('mahasiswa/profil');
    }

    public function absensi()
    {
        $this->load->view('mahasiswa/absensi');
    }

    public function tugas()
    {
        $this->load->view('mahasiswa/tugas');
    }

    public function tugas_detail2()
    {
        $this->load->view('mahasiswa/tugas_detail2');
    }

    public function tugas_detail()
    {
        $this->load->view('mahasiswa/tugas_detail');
    }

    public function ujian()
    {
        $this->load->view('mahasiswa/ujian');
    }

    public function ujian_detail()
    {
        $this->load->view('mahasiswa/ujian_detail');
    }

    public function ujian_detail2()
    {
        $this->load->view('mahasiswa/ujian_detail2');
    }

    public function materi()
    {
        $this->load->view('mahasiswa/materi');
    }

    public function materi_detail()
    {
        $this->load->view('mahasiswa/materi_detail');
    }

    public function registration()
    {
        $this->load->view('mahasiswa/registration');
        $this->load->view('template/footer');
    }
}
