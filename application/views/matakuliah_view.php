<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card-body">
					<h2 class="card-title">Manajemen Mata Kuliah </h2>
					<hr>
					<a href="<?= base_url('admin/add_mhs') ?>" class="btn btn-outline-primary">Tambah
						Data Mata Kuliah ⭢</a>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover" id="save-stage" style="width:100%;float:right;">
									<thead>
										<tr>
											<th>Kode MK</th>
											<th>Nama Matakuliah</th>
											<th>Nama Dosen</th>
											<th>Jurusan</th>
											<th>Semester</th>
											<th colspan="2">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php

										foreach ($user as $u) {
										?>
											<tr>

												<th scope="row">
													<?php echo $u->kodemk ?>
												</th>

												<td>
													<?php echo $u->namamk ?>
												</td>
												<td>
													<?php echo $u->iddosen ?>
												</td>
												<td>
													<?php echo $u->namadosen ?>
												</td>
												<td>
													<?php echo $u->prodi ?>
												</td>
												<td>
													<?php echo $u->semester ?>
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('components/foot'); ?>
<script src="assets/js/app/myfunction.js"></script>
<script src="assets/js/app/matakuliah.js"></script>

<?php $this->load->view('components/jsfoot2'); ?>

</body>

</html>

<!-- 
		<script src="assets/js/app/myfunction.js"></script>
		<script src="assets/js/app/matakuliah.js"></script> -->

<!-- <div class="card-body">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Modal
				With Form</button>
		</div> -->
<!-- modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" id="form-matakuliah" data-backdrop="false" aria-labelledby="formModal" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="mode">Tambah Matakuliah</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" action="">
							<div class="form-group">
								<label for="kodemk">kodemk</label>
								<div class="input-group">
									<input type="text" id="kodemk" name="kodemk" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label for="namamk">Nama Matakuliah</label>
								<div class="input-group">
									<input type="text" id="namamk" name="namamk" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label for="prodi">Program Studi</label>
								<div class="input-group" id="val1">
									<select id="prodi" name="prodi" class="form-control">
										<option value="" disabled selected>Pilih</option>
										<option value="TI">Teknik Informatika</option>
										<option value="SI">Sistem Informasi</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="semester">Semester</label>
								<div class="input-group">
									<select id="semester" name="semester" class="form-control">
										<option value="#" disabled selected>Pilih</option>
										<option value="1">I</option>
										<option value="2">II</option>
										<option value="3">III</option>
										<option value="4">IV</option>
										<option value="5">V</option>
										<option value="6">VI</option>
										<option value="7">VII</option>
										<option value="8">VIII</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label>Dosen</label>
								<div class="input-group">
									<div class="controls">
										<select class="span3" name="iddosen" id="iddosen">
											<option disabled selected>Pilih</option>
											<?php
											$dosen = $this->db->query("select * from tbladmin where status = 'dosen'");
											foreach ($dosen->result() as $row1) {
												echo "<option data-nama='" . $row1->namaadmin . "' value='" . $row1->idadmin . "'>" . $row1->namaadmin . "</option>";
											}
											?>
										</select>
										<input type="hidden" id="namadosen" name="namadosen" class="form-control">
									</div>
								</div>
								<button type="button" class="btn btn-primary m-t-15 waves-effect" id="simpan">Simpan</button>
								<button type="button" class="btn btn-danger m-t-15 waves-effect">Batal</button>
						</form>
					</div>
				</div>
			</div>
		</div> -->
<!-- <div class="modal hide fade" tabindex="-1" role="dialog" id="form-matakuliah">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h3>Form <span id="mode"></span> matakuliah</h3>
					</div>
					<div class="modal-body">
	
						<form class="form-horizontal" action="">
							<div class="control-group" id="tkdmk" style="display:none;">
								<label class="control-label" for="kodemk">kodemk</label>
								<div class="controls">
									<input type="text" id="kodemk" name="kodemk" class="form-control">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="namamk">Nama Matakuliah</label>
								<div class="controls">
									<input type="text" id="namamk" name="namamk" class="form-control">
								</div>
							</div>
							<div class="control-group">
							<label class="control-label" for="sks">SKS</label>
							<div class="controls">
								<input type="text" id="sks" name="sks" class="form-control">
							</div>
						</div>
							<div class="control-group">
								<label class="control-label" for="prodi">Program studi</label>
								<div class="controls" id="val1">
									<select id="prodi" name="prodi" class="form-control">
										<option value="" disabled selected>Pilih</option>
										<option value="TI">Teknik Informatika</option>
										<option value="SI">Sistem Informasi</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="semester">Semester</label>
								<div class="controls">
									<select id="semester" name="semester" class="form-control">
										<option value="#" disabled selected>Pilih</option>
										<option value="1">I</option>
										<option value="2">II</option>
										<option value="3">III</option>
										<option value="4">IV</option>
										<option value="5">V</option>
										<option value="6">VI</option>
										<option value="7">VII</option>
										<option value="8">VIII</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Dosen</label>
								<div class="controls">
									<select class="span3" name="iddosen" id="iddosen">
										<option disabled selected>Pilih</option>
										<?php
										$dosen = $this->db->query("select * from tbladmin where status = 'dosen'");
										foreach ($dosen->result() as $row1) {
											echo "<option data-nama='" . $row1->namaadmin . "' value='" . $row1->idadmin . "'>" . $row1->namaadmin . "</option>";
										}
										?>
									</select>
									<input type="hidden" name="namadosen" id="namadosen">
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn btn-success" id="simpan">
							<span class="icon-floppy-disk"></span> Simpan</button>
						<button class="btn btn-danger" data-dismiss="modal">
							<span class="icon-remove"></span> Batal</button>
					</div>
				</div>
			</div>
		</div>  -->
<!-- ./modal -->
</body>

</html>