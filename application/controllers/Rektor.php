<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rektor extends CI_Controller
{

	public function index()
	{
		$this->load->view('rektor/index');
	}



// Profil Rektor

    public function profil_rektor()
    {
        $this->load->view('rektor/profil_rektor');
    }

// Fakultas Dirosah Islamiyah

    public function fdi_sii()
    {
        $this->load->view('rektor/fdi_sii');
    }

// Fakultas Ekonomi

    public function fe_akt()
    {
        $this->load->view('rektor/fe_akt');
    }

    public function fe_mnj()
    {
        $this->load->view('rektor/fe_mnj');
    }

// Fakultas Industri Halal

    public function fih_agr()
    {
        $this->load->view('rektor/fih_agr');
    }

    public function fih_farm()
    {
        $this->load->view('rektor/fih_farm');
    }

    public function fih_thp()
    {
        $this->load->view('rektor/fih_thp');
    }

// Fakultas Ilmu Pendidikan

    public function fip_pbi()
    {
        $this->load->view('rektor/fip_pbi');
    }

    public function fip_pgsd()
    {
        $this->load->view('rektor/fip_pgsd');
    }

// Fakultas Teknologi Informasi

    public function fti_inf()
    {
        $this->load->view('rektor/fti_inf');
    }

    public function fti_te()
    {
        $this->load->view('rektor/fti_te');
    }

    public function fti_tk()
    {
        $this->load->view('rektor/fti_tk');
    }


// Pesan Untuk Rektor

    public function rektor_tulis_pesan()
    {
        $this->load->view('rektor/rektor_tulis_pesan');
    }

    public function rektor_baca_pesan()
    {
        $this->load->view('rektor/rektor_baca_pesan');
    }

    public function rektor_pesan_masuk()
    {
        $this->load->view('rektor/rektor_pesan_masuk');
    }



}
