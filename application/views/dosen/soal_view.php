<?php $this->load->view('components/head2'); ?>
<base href="<?= base_url(); ?>">
<?php $this->load->view('components/navbardosen'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;">Soal</h1>
		</div>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span3">
					<div class="widget-box">
						<div class="widget-title"> <span class="icon"> <i class="icon-list"></i> </span>
						</div>
						<div class="widget-content">
							<a href="subbuatsoal" class="thumbnail">
								<img src="assets/img/icons/031-q&a.png" alt="soal">
								<div class="caption">
									<h4 align="center" class="textsub">Tambah soal</h4>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="span3">
					<div class="widget-box">
						<div class="widget-title"> <span class="icon"> <i class="icon-list"></i> </span>
						</div>
						<div class="widget-content">
							<a href="lihatsoal" class="thumbnail">
								<img src="assets/img/icons/038-laptop.png" alt="soal">
								<div class="caption">
									<h4 align="center" class="textsub">Lihat soal</h4>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="span6">
					<img src="assets/img/gambar1up.jpg" alt="" style="margin-top:15px;">
				</div>
			</div>
		</div>
</div>
</div>

<?php $this->load->view('components/foot'); ?>
<?php $this->load->view('components/jsfoot2'); ?>
</body>

</html>