<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Menggunakan CSS dan JSnya Okta -->
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Admin UNU Yogyakarta</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/css/app.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/bootstrap-social/bootstrap-social.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/jqvmap/dist/jqvmap.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/css/style.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/css/components.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/css/custom.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/dropzonejs/dropzone.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/summernote/summernote-bs4.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/codemirror/lib/codemirror.css">
  <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/codemirror/theme/duotone-dark.css"> -->
  <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/jquery-selectric/selectric.css"> -->
  <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"> -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>assets2/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">


  <link rel='shortcut icon' type='image/x-icon' href="<?= base_url('assets2/') ?>img/unu.png">

</head>