<?php $this->load->view('components/head2'); ?>
<base href="<?= base_url(); ?>">
<?php $this->load->view('components/navbardosen'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;"><span class="icon-briefcase"></span>
			Lihat Materi<small> Pembelajaran Online</small></h1>
		</div>
	<hr>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="quick-actions_homepage offset2">
				<ul class="quick-actions">
					<li class="bg_lo span3"> <a href="dosen/lihatup"> <i class="icon-book"></i>Materi PDF </a> </li>
					<li class="bg_lg span3"> <a href="dosen/lihatdis"> <i class="icon-th-list"></i>Materi Diskusi</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<hr>
	<div class="container-fluid">
	<div class="ini"></div>
		<div class="row-fluid">
			<div class="span12">
				<!-- <div class="accordion" id="collapsefilter">
					<div class="accordion-group" style="width:400px;">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#collapsefilter"
								href="#tampilfilter">
								<strong>Filter Data <span class="icon icon-chevron-down"></span></strong>
							</a>
						</div>
						<div id="tampilfilter" class="accordion-body collapse">
							<div class="accordion-inner">
								<form class="form-inline">
									<table>
										<tr>
											<td><label for="semester">Semester</label></td>
											<td>
												<select class="span3" id="semester" name="semester">
													<option value="1">I</option>
													<option value="2">II</option>
													<option value="3">III</option>
													<option value="4">IV</option>
													<option value="5">V</option>
													<option value="6">VI</option>
													<option value="7">VII</option>
													<option value="8">VIII</option>
												</select>
											</td>
										</tr>
										<tr>
											<td> <label for="judulmateri">Judul Materi</label></td>
											<td><input type="text" id="judulmateri" name="judulmateri"
													placeholder="Optional"></td>
										</tr>
										<tr>
											<td>&nbsp</td>
											<td><button type="submit" id="filter" class="btn">Filter</button></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div> -->
				<div class="widget-box">
					<button class="btn btn-success pull-right" onclick="location.href='dosen/subuploadmateri'"
						style="margin:3px 5px;">
						<span class="icon-plus"></span> Tambah</button>
					<div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
						<h5>Lihat materi uplaod</h5>
					</div>
					<div class="widget-content nopadding">
						<table class="table table-bordered table-striped ">
							<thead>
								<tr>
									<th style="width:50px;" align='center'>ID Materi</th>
									<th style="width:auto;">Matakuliah</th>
									<th>Judul Materi</th>
									<th>Tanggal</th>
									<th>File</th>
									<th colspan="2">Action</th>
								</tr>
							</thead>
							<tbody id="tblup">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>






<?php $this->load->view('components/foot'); ?>
<script src="assets/js/app/myfunction.js"></script>
<script src="assets/js/app/dosen/lihatup.js"></script>

<?php $this->load->view('components/jsfoot2'); ?>

</body>

</html>