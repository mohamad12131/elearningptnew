<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>

<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;">Dashboard</h1>
		</div>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12">
					<div class="accordion" id="collapsefilter">
						<div class="accordion-group" style="width:400px;">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#collapsefilter" href="#tampilfilter">
									<strong>Filter Data <span class="icon icon-chevron-down"></span></strong>
								</a>
							</div>
							<div id="tampilfilter" class="accordion-body collapse in">
								<div class="accordion-inner">
									<form class="form-inline">
										<table style="width:400px;">
											<tr>
												<td><label for="matakuliahfil">Matakuliah</label></td>
												<td>
													<select name="matakuliahfil" id="matakuliahfil" class="span11">
														<option value="" disabled selected>Pilih</option>
														<?php
														$namamk = $this->db->query("select * from tblmatakuliah");
														foreach ($namamk->result() as $row) {
															echo "<option value='" . $row->kodemk . "'>" . $row->namamk . " - " . $row->prodi . " - " . $row->semester . "</option>";
														}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td><label for="kelasfil">Kelas</label></td>
												<td>
													<select id="kelasfil" name="kelasfil" class="form-control">
														<option value="" disabled selected>Pilih</option>
														<option value="P1">P1</option>
														<option value="P2">P2</option>
														<option value="M1">M1</option>
													</select>
												</td>
											</tr>
											<tr>
												<td>&nbsp</td>
												<td>
													<button type="submit" id="filter" class="btn">Filter</button>
													<!-- <button type="submit" id="filterall" class="btn">Tampil Semua</button> -->
												</td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="widget-box">
						<div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
							<h5>Data Absensi Mahasiswa Pembelajaran Online</h5>
						</div>

						<div class="widget-content nopadding">
							<table class="table table-bordered table-striped ">
								<thead>
									<tr>
										<th>NIM</th>
										<th>Nama</th>
										<th>Team</th>
										<th>a1</th>
										<th>a2</th>
										<th>a3</th>
										<th>a4</th>
										<th>a5</th>
										<th>a6</th>
										<th>a7</th>
										<th>a8</th>
										<th>a9</th>
										<th>a10</th>
										<th>a11</th>
										<th>a12</th>
										<th>a13</th>
										<th>a14</th>
										<th>a15</th>
										<th>a16</th>
										<!-- <th align="center">Action</th> -->
									</tr>
								</thead>
								<tbody id="tabelabsen">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->load->view('components/foot'); ?>

<script src="assets/js/app/myfunction.js"></script>
<script src="assets/js/app/absensi.js"></script>
<?php $this->load->view('components/jsfoot2'); ?>
</body>

</html>