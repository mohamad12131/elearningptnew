<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>
<!--main-container-part-->
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card-body">
					<h2 class="card-title">Soal </h2>
					<hr>
					<a href="<?= base_url('subbuatsoal') ?>" class="btn btn-outline-primary">
						Tambah Soal</a>
					<a href="<?= base_url('lihatsoal') ?>" class="btn btn-outline-primary">
						Lihat Soal</a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->load->view('components/foot'); ?>


<?php $this->load->view('components/jsfoot2'); ?>
</body>

</html>