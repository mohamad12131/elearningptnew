<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="<?= base_url('assets2/') ?>img/unu-1.png" type="image/png">
	<!-- Title -->
	<title>
		Hai,
		<?= $this->session->userdata("ses_nama"); ?>
		<?php if ($this->session->userdata("islogin") == 'masukmhs') { ?>
		<?php } ?>
	</title>
	<!-- Bootstrap CSS -->
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>css/bootstrap.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/linericon/style.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/lightbox/simpleLightbox.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/animate-css/animate.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/popup/magnific-popup.css">
	<!-- Main css -->
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>css/style.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>css/user_style.css">
	<link rel="stylesheet" href="<?= base_url('assets2/') ?>css/responsive.css">
	<!-- Fonts -->
	<!-- Template CSS -->
	<!-- <link rel="stylesheet" href="<?= base_url('assets2/') ?>stisla-assets/css/style.css"> -->
	<!-- <link rel="stylesheet" href="<?= base_url('assets2/') ?>stisla-assets/css/components.css"> -->
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
	<!-- Library -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.4/dist/sweetalert2.all.min.js"></script>

	<!-- Dosen Link -->

	<!--begin::Fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="<?= base_url('assets2') ?>/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles -->

	<!--begin:: Global Mandatory Vendors -->
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

	<!--end:: Global Mandatory Vendors -->

	<!--begin:: Global Optional Vendors -->
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets2') ?>/assets/vendors/custom/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

	<!--end:: Global Optional Vendors -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="<?= base_url('assets2') ?>/assets/demo/demo7/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->

	<!--end::Layout Skins -->
	<!-- <link rel="shortcut icon" href="<?= base_url('assets2') ?>/img/favicon.png" /> -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.0/dist/sweetalert2.all.min.js"></script>

</head>

<body style="overflow-x:hidden;background-color:#fbf9fa">



	<!-- Start Navigation Bar -->
	<div class="container">
		<header class="header_area" style="background-color: white !important; ">
			<div class="main_menu">
				<nav class="navbar navbar-expand-lg navbar-light">

					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="<?= base_url('welcome') ?>"><img src="<?= base_url('assets2/') ?>img/unu-2.png" alt="" style="width: 225px; height: 55px; margin-left: 32px;"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent" style="margin-right: 32px;">
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item ">
								<a class="nav-link" href="<?= base_url('hmahasiswa/profil') ?>">Profil
								</a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="<?= base_url('hmahasiswa/home') ?>">Beranda</a>
							<li class="nav-item">
								<a class="nav-link" href="<?= base_url('hmahasiswa/materimhs') ?>">Materi</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?= base_url('hmahasiswa/absensimhs') ?>">Absensi</a>
							<li class="nav-item">
								<a class="nav-link" href="<?= base_url('hmahasiswa/soalmhs') ?>">Tugas</a>
							<li class="nav-item">
								<a class="nav-link" href="<?= base_url('hmahasiswa/nilaimhs') ?>">Nilai</a>
							<li class="nav-item">
								<a class="nav-link" href="<?= base_url('hmahasiswa/materimhs') ?>">Forum Diskusi</a>
							</li>
							<li class=" nav-item " style="color: red;">
								<a class=" nav-link text-danger" href="<?= base_url('welcome/logout') ?>">Keluar</a>
							</li>
						</ul>
					</div>
			</div>
			</nav>
	</div>
	</header>
	<!-- End Navigation Bar -->
	<div class="container">
		<!-- <div class="bg-white mx-auto p-4 buat-text" data-aos="fade-down" data-aos-duration="1400" style="width: 100%; border-radius:32px;"> -->
		<div class="row" style="color: black; font-family: 'poppins';">
			<!-- <div class="col-md-12 mt-1"> -->
			<div class="main-content">


				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">


					<!-- begin:: Content -->

					<!-- begin:: Content -->
					<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

						<!--Begin::Dashboard 7-->

						<!--Begin::Section-->

						<div class="row" style="margin-top: 89px;">
							<div class="col-xl-12">

								<!--begin:: Widgets/Blog-->
								<div class="kt-portlet kt-portlet--height-fluid kt-widget19" style="border-radius: 8px;">
									<div class="kt-portlet__body kt-portlet__body--fit">
										<div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides">
											<img src="<?= base_url('assets2/') ?>img/user.png" class=" img-fluid" alt="" srcset="" style="border-radius: 8px 8px 0 0;">
											<h4 class="kt-widget19__title kt-font-light display-4 font-weight-bold" style="letter-spacing: -1px;">
												Selamat Belajar

												<?= $this->session->userdata("ses_nama"); ?>
												<?php if ($this->session->userdata("islogin") == 'masukmhs') { ?>
												<?php } ?>
											</h4>
											<div class="kt-widget19__shadow"></div>
											<div class="kt-widget19__labels" style="margin-top: -19px">

												<div class="kt-subheader   kt-grid__item" id="kt_subheader" style="background: none; margin-left: -28px;">
													<div class="kt-subheader__main">
													</div>
													<div class="kt-subheader__toolbar ">
														<div class="kt-subheader__wrapper">
															<a href="#" class="btn kt-subheader__btn-daterange">
																<span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Tanggal</span>&nbsp;
																<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date"><?php echo date('d - M - y'); ?></span>

																<!--<i class="flaticon2-calendar-1"></i>-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--sm">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect id="bound" x="0" y="0" width="24" height="24" />
																		<path d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z" id="check" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																		<path d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z" id="Combined-Shape" fill="#000000" />
																	</g>
																</svg>
															</a>
														</div>
													</div>
												</div>
												<a href="<?= base_url('user/profil') ?>" class="btn btn-label-light-o2 btn-bold btn-sm ">Lengkapi Data</a>

											</div>

										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-widget19__wrapper">
											<div class="kt-widget19__content">
												<div class="kt-widget19__userpic">
													<img src="<?= base_url('assets2/') ?>assets/media/users/default.jpg" alt="">
												</div>
												<div class="kt-widget19__info">
													<a href="#" class="kt-widget19__username font-weight-bold">
														Okta Brahvila Karim
													</a>
													<span class="kt-widget19__time">
														Mahasiswa | Informatika
													</span>
												</div>
											</div>
											<div class="kt-widget19__text">
												Selamat datang di halaman guru Learnify, anda dapat menambah materi .
												Dalam materi anda dapat memasukan video, dan deskripsi nya. Seemoga anda
												dapat menikmati Learnify!, kontak Administrator jika terjadi masalah
												apapun yang terkait upload materi. Terima kasih telah menggunakan
												learnify!
												<br>
												Selamat Bekerja :)
											</div>
										</div>
										<div class="kt-widget19__action">
											<a href="#" class="btn btn-sm btn-label-brand btn-bold">Tambah Materi</a>
										</div>
									</div>
								</div>

								<!--end:: Widgets/Blog-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End::Section-->

		<div class="container" style="margin-top: -32px;">
			<div class="row mt-4 mb-5 justify-content-center">
				<div class="col-md-4 bg-white p-3" id="detail" style="border-radius:8px;box-shadow:rgba(0, 0, 0, 0.03) 0px 4px 8px 0px; margin-right: 21px;">
					<h1 class="font-weight-bold card-title text-center" style="color: black;">
						<!-- <?= $detail->nama ?> -->
					</h1>
					<h6 class="font-weight-bold" style="line-height: 5px; text-align: left;">
						Kehadiran
					</h6>
					<hr>
					<table style="width: 100%" class="container text-center">
						<tbody>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Nama Siswa :</span></td>
								<td>
									<?= $profil->namamhs ?>

									<!-- <?= $detail->nama ?> -->
								</td>
							</tr>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Alamat Email :</span></td>
								<td>
									<!-- <?= $detail->email ?> -->
								</td>
							</tr>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Password : </span></td>
								<td>Terenkripsi</td>
							</tr>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Akun Aktif? :</span></td>
								<td>
									<!-- <?= $detail->is_active ?> -->
								</td>
							</tr>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Terdaftar pada :</span></td>
								<td>
									<!-- <?= $detail->date_created ?> -->
								</td>
							</tr>
						</tbody>
					</table>
					<p style="font-weight:500px!important;font-size:18px;text-align:justify;" class="text-justify">
					</p>
					<a href="<?= base_url('admin/data_siswa') ?>" class="btn btn-success btn-block" style=" border-radius: 8px;">Cek Kehadiran</a>
				</div>

				<div class="col-md-7 bg-white p-3" id="detail" style="border-radius:8px; box-shadow:rgba(0, 0, 0, 0.03) 0px 4px 8px 0px;">
					<h1 class="font-weight-bold card-title text-center" style="color: black;">
						<!-- <?php echo $row['namamhs']; ?> -->
					</h1>
					<h6 class="font-weight-bold" style="line-height: 5px; text-align: left;">
						Tugas
					</h6>
					<hr>
					<table style="width: 100%" class="container text-center">
						<tbody>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Nama Siswa :</span></td>
								<td>
									<?= $detail->namamhs ?>
								</td>
							</tr>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Alamat Email :</span></td>
								<td>
									<!-- <?= $detail->email ?> -->
								</td>
							</tr>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Password : </span></td>
								<td>Terenkripsi</td>
							</tr>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Akun Aktif? :</span></td>
								<td>
									<!-- <?= $detail->is_active ?> -->
								</td>
							</tr>
							<tr style="border-bottom: 0.5px solid #6c757d;">
								<td><span class="font-weight-bold">Terdaftar pada :</span></td>
								<td>
									<!-- <?= $detail->date_created ?> -->
								</td>
							</tr>
						</tbody>
					</table>
					<p style="font-weight:500px!important; font-size:18px; text-align:justify;" class="text-justify">
					</p>
					<a href="<?= base_url('admin/data_siswa') ?>" class="btn btn-success btn-block" style=" border-radius: 8px;">Cek Tugas</a>
				</div>
			</div>
		</div>
		<!-- Start Greetings Card -->
		<div class="container" style="margin-top: 32px;">
			<div class="bg-white mx-auto p-4 buat-text" data-aos="fade-down" data-aos-duration="1400" style="width: 100%; border-radius:8px;">
				<div class="row" style="color: black; font-family: 'poppins';">
					<div class="col-md-12 mt-1">
						<h1 class="display-4" style="color: black; font-family:'poppins'; font-size: 32px;" data-aos="fade-down" data-aos-duration="1400">Yuk, belajar lebih banyak lagi !<span style="font-size: 32px;">
							</span> </h1>
						<p>Hai, dibawah ini Terdapat pembelajaran dari semester satu hingga semester delapan
							<br> Silahkan pilih semester yang akan kamu
							akses,
							Selamat belajar!
						</p>
						<hr>

						<h4 data-aos="fade-down" data-aos-duration="1800">Silahkan pilih semester yang ingin kamu akses
							dibawah
							ini!
						</h4>
						<!-- Start Class Card -->
						<div class="container">
							<div class="row mt-4 mb-5 justify-content-center">
								<div class="col-md-12">
									<div class="row">
										<div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-center " data-aos-duration="1900" data-aos="fade-right" width="215" height="100">
											<a href="<?= base_url('user/kelas10') ?>">
												<div class="card-kelas text-center" width="215" height="100">
													<img src="<?= base_url('assets2/') ?>img/s-1.png" style="object-fit: cover; border-radius: 8px; height: 100px; width: 215px;" class="card-img-top img-fluid" alt="...">
												</div>
											</a>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-center " data-aos-duration="1900" data-aos="fade-down">
											<a href="<?= base_url('user/kelas11') ?>">
												<div class="card-kelas">
													<img src="<?= base_url('assets2/') ?>img/s-2.png" class="card-img-top" alt="..." style=" border-radius: 8px; height: 100px; width: 215px;">
												</div>
											</a>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-center" data-aos-duration="1900" data-aos="fade-left">
											<a href="<?= base_url('user/kelas12') ?>">
												<div class="card-kelas">
													<img src="<?= base_url('assets2/') ?>img/s-3.png" class="card-img-top" alt="..." style=" border-radius: 8px; height: 100px; width: 215px;">
												</div>
											</a>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-center" data-aos-duration="1900" data-aos="fade-left">
											<a href="<?= base_url('user/kelas12') ?>">
												<div class="card-kelas">
													<img src="<?= base_url('assets2/') ?>img/s-4.png" class="card-img-top" alt="..." style=" border-radius: 8px; height: 100px; width: 215px;">
												</div>
											</a>
										</div>
									</div>
									<div class="row" style="margin-top: 32px;">
										<div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-center " data-aos-duration="1900" data-aos="fade-right" width="215" height="100">
											<a href="<?= base_url('user/kelas10') ?>">
												<div class="card-kelas text-center" width="215" height="100">
													<img src="<?= base_url('assets2/') ?>img/s-5.png" style="object-fit: cover; border-radius: 8px; height: 100px; width: 215px;" class="card-img-top img-fluid" alt="...">
												</div>
											</a>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-center " data-aos-duration="1900" data-aos="fade-down">
											<a href="<?= base_url('user/kelas11') ?>">
												<div class="card-kelas">
													<img src="<?= base_url('assets2/') ?>img/s-6.png" class="card-img-top" alt="..." style=" border-radius: 8px; height: 100px; width: 215px;">
												</div>
											</a>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-center" data-aos-duration="1900" data-aos="fade-left">
											<a href="<?= base_url('user/kelas12') ?>">
												<div class="card-kelas">
													<img src="<?= base_url('assets2/') ?>img/s-7.png" class="card-img-top" alt="..." style=" border-radius: 8px; height: 100px; width: 215px;">
												</div>
											</a>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-center" data-aos-duration="1900" data-aos="fade-left">
											<a href="<?= base_url('user/kelas12') ?>">
												<div class="card-kelas">
													<img src="<?= base_url('assets2/') ?>img/s-8.png" class="card-img-top" alt="..." style=" border-radius: 8px; height: 100px; width: 215px;">
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End Class Card -->

					</div>
				</div>
			</div>
		</div>
		<!-- begin:: Footer -->
		<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" style="margin-top: 34px; border-radius: 8px;">
			<div class="kt-footer__copyright">
				2021&nbsp;&copy;&nbsp;<a href="https://syauqizaidan.github.io/" target="_blank" class="kt-link">Advanced Informatics</a>
			</div>
			<div class="kt-footer__menu">
				Made with &nbsp; <span class="" style="color: red"> &#10084;</span> &nbsp; by Advanced Informatics
			</div>
		</div>

		<!-- end:: Footer -->
		<!-- End Greetings Card -->


		<br>

		<br>


		<!-- Start Animate On Scroll -->
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
		<script>
			AOS.init();
		</script>
		<!-- End Animate On Scroll 