<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="Learnify dibuat ditujukan agar para siswa dan guru dapat terus belajar dan mengajar dimana saja dan kapan saja." name="Description" />
    <meta content="Learnify, E-learning, Open Source, Syauqi Zaidan Khairan Khalaf, github, programmer indonesia" name="keywords" />
    <link rel="icon" href="<?= base_url('assets2/') ?>img/unu-1.png" type="image/png">
    <title>Learntara - UNU Yogyakarta</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/linericon/style.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/animate-css/animate.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/popup/magnific-popup.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/style.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/responsive.css">

    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/breadcrumb.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/_header.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/_footer.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/_testimonials.css">

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <!-- Scripts -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.4/dist/sweetalert2.all.min.js"></script>
    <script src="<?= base_url('assets2/') ?>js/jquery-3.3.1.min.js"></script>
    <script src="<?= base_url('assets2/') ?>js/popper.js"></script>
    <script src="<?= base_url('assets2/') ?>js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(() => {
            $("#nav<?= $this->uri->segment(2); ?>").addClass('active')
        })
    </script>

</head>

<body>

    <!--================Header Menu Area =================-->
    <header class="header_area">
        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="<?= base_url('index') ?>"><img src="<?= base_url('assets2/') ?>img/unu-2.png" alt="" width="225" height="55"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <li class="nav-item"><a class="nav-link active" href="#" data-toggle="modal" data-target="#exampleModalCenter" style="color: #000;">Masuk</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!--================ END Header Menu Area =================-->

    <!--================Home Banner Area =================-->
    <section class="home_banner_area" style="margin-top: 78px;">
        <div class="banner_inner d-flex align-items-center">
            <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background="">
            </div>
            <div class="container">
                <div class="banner_content text-center" style="margin-top: 0px;">
                    <h3 data-aos="fade-up" data-aos-duration="1600">Belajar Dimana & Kapan Saja <br /> Bersama Learntara</h3>
                    <p data-aos="fade-up" data-aos-duration="1900">Dengan Learntara kemudahan kegiatan belajar mengajar dapat terpenuhi. Dosen dan mahasiswa dapat
                        belajar meski banyak halangan atau rintangan. Nikmati Pembelajaran terstruktur dan efektif menggunakan Learntara serta kemudahan belajar dengan menggunakan aplikasi kami. </p>
                </div>
            </div>
        </div>
    </section>
    <!-- ================End Home Banner Area ================= -->

    <!--================Finance Area =================-->
    <section class="finance_area">
        <div class="container">
            <div class="finance_inner row">
                <div class="col-lg-3 col-sm-6">
                    <div class="finance_item">
                        <div data-aos="fade-right" data-aos-duration="1600" class="media">
                            <div class="d-flex">
                                <i class="lnr lnr-users"></i>
                            </div>
                            <div class="media-body">
                                <h5>Absensi, materi dan perkuliahan</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="finance_item">
                        <div data-aos="fade-right" data-aos-duration="1800" class="media">
                            <div class="d-flex">
                                <i class="lnr lnr-file-empty"></i>
                            </div>
                            <div class="media-body">
                                <h5>Upload tugas, materi dan ujian</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-right" data-aos-duration="2000" class="col-lg-3 col-sm-6">
                    <div class="finance_item">
                        <div class="media">
                            <div class="d-flex">
                                <i class="lnr lnr-camera-video"></i>
                            </div>
                            <div class="media-body">
                                <h5>Pembelajaran melalui video</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-right" data-aos-duration="2200" class="col-lg-3 col-sm-6">
                    <div class="finance_item">
                        <div class="media">
                            <div class="d-flex">
                                <i class="lnr lnr-tag"></i>
                            </div>
                            <div class="media-body">
                                <h5>Perkuliahan praktis, simpel dan fleksibel</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Finance Area =================-->

    <!--================ Illustrations Area =================-->
    <section class="lernify-for-indonesia p_20">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <img data-aos="fade-up" data-aos-duration="1800" src="<?= base_url('assets2/') ?>img/illustrations/index-study.svg" alt="" srcset="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 mx-auto">
                    <div class="main_title">
                        <h2 data-aos="fade-up" data-aos-duration="2000">Learntara Dibuat Untuk Meningkatkan Kualitas Pembelajaran Di Indonesia</h2>
                        <!-- <p data-aos="fade-up" data-aos-duration="2200">Sistem Informasi Pembelajaran berbasis Website. Dibuat guna membantu Universitas dalam memudahkan kegiatan belajar mengajar. Sangat efektif untuk penugasan dan berbagi materi antar program studi.</p> -->
                        <a href="https://github.com/syauqi/Learntara"><button data-aos="fade-up" data-aos-duration="2400" class="main_btn" style="margin-top: 32px; border: none; background-color: #4DBF1C; color: #fff;">Download Learntara</span></button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Illustrations Area =================-->

    <!--================Courses Area =================-->
    <section class="team_area p_20">
        <div class="container">
            <div class="main_title">
                <h2 data-aos="fade-up" data-aos-duration="1600">Belajar Bersama Learntara</h2>
                <p data-aos="fade-up" data-aos-duration="1800">Kamu dapat belajar materi pembelajaran dari fakultas dan prodi lain di kampusmu. Lihat pembelajaran program studi lain agar dapat terintegritas dengan program studi kamu. Dengan materi yang dibagika oleh setiap prodi kamu dapat belajar berbagai macam ilmu pengetahuan.</p>
            </div>
            <div class="row courses_inner">
                <div class="col-lg-9">
                    <div class="grid_inner">
                        <div class="grid_item wd55">
                            <div class="courses_item" data-aos="fade-right" data-aos-duration="1800">
                                <img src="<?= base_url('assets2/') ?>img/courses/fti-1.png" alt="" style="border-radius: 13px;" width="458" height="220">
                                <div class="hover_text">
                                    <a href="javaScript:void(0);">
                                        <h4>Fakultas Teknologi Informasi</h4>
                                    </a>
                                    <ul class="list">
                                        <li><a href="#"><i class="lnr lnr-user"></i>Kharis Yugi Fatkhurrohman, S.T</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="grid_item wd44">
                            <div class="courses_item" data-aos="fade-down" data-aos-duration="1800">
                                <img src="<?= base_url('assets2/') ?>img/courses/fip-1.png" alt="" style="border-radius: 13px;" width="360" height="220">
                                <div class="hover_text">
                                    <a href="javaScript:void(0);">
                                        <h4>Fakultas Ilmu Pendidikan</h4>
                                    </a>
                                    <ul class="list">
                                        <li><a href="#"><i class="lnr lnr-user"></i>Mohamad Hatami, S.Kom</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="grid_item wd44">
                            <div class="courses_item" data-aos="fade-right" data-aos-duration="1800">
                                <img src="<?= base_url('assets2/') ?>img/courses/fih-1.png" alt="" style="border-radius: 13px;" width="360" height="220">
                                <div class="hover_text">
                                    <a href="javaScript:void(0);">
                                        <h4>Fakultas Industri Halal</h4>
                                    </a>
                                    <ul class="list">
                                        <li><a href="#"><i class="lnr lnr-user"></i> Okta Brahvila Karim, S.Farm</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="grid_item wd55">
                            <div class="courses_item" data-aos="fade-up" data-aos-duration="1800">
                                <img src="<?= base_url('assets2/') ?>img/courses/fe-1.png" alt="" style="border-radius: 13px; " width="458" height="220">
                                <div class="hover_text">
                                    <a href="javaScript:void(0);">
                                        <h4>Fakultas Ekonomi</h4>
                                    </a>
                                    <ul class="list">
                                        <li><a href="#"><i class="lnr lnr-user"></i> Isra Rustam, S.E</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="course_item" data-aos="fade-left" data-aos-duration="1800">
                        <img src="<?= base_url('assets2/') ?>img/courses/di-1.png" alt="" style="border-radius: 13px;" width="262" height="470">
                        <div class="hover_text">
                            <a href="javaScript:void(0);">
                                <h4>Fakultas Dirosah Islamiyah</h4>
                            </a>
                            <ul class="list">
                                <li><a href="#"><i class="lnr lnr-user"></i>Ahmad Semsudin, S.Ag</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Courses Area =================-->

    <!--================Team Area =================-->
    <section class="courses_area p_40">
        <div class="container">
            <div class="main_title">
                <h2 data-aos="fade-up" data-aos-duration="1800">Learntara didukung oleh:</h2>
            </div>
            <section class="testimonials_area p_20">
                <div class="container">
                    <div class="testi_slider owl-carousel">
                        <div class="item">
                            <div class="testi_item" style="border-radius: 13px;">
                                <img src="<?= base_url('assets2/') ?>img/testimonials/rektor.png" alt="" height="130" width="130">
                                <h4>Prof. Drs. Purwo Santoso, M.A., Ph.D.</h4>
                                <ul class="list">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                                <p>Merciful revaluation burying love ultimate value inexpedient ubermensch. Holiest madness victorious morality hope endless christian madness. Love dead fearful transvaluation marvelous. Oneself right ideal abstract endless faith deceptions zarathustra grandeur law ubermensch free. Abstract chaos snare play truth ultimate good self. God overcome sexuality pious abstract good decieve revaluation aversion good. Virtues chaos overcome society holiest truth.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testi_item" style="border-radius: 13px;">
                                <img src="<?= base_url('assets2/') ?>img/testimonials/unu-3.png" alt="" height="160" width="160">
                                <h4>Universitas Nahdlatul Ulama Yogyakarta</h4>
                                <ul class="list">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                                <p>Merciful revaluation burying love ultimate value inexpedient ubermensch. Holiest madness victorious morality hope endless christian madness. Love dead fearful transvaluation marvelous. Oneself right ideal abstract endless faith deceptions zarathustra grandeur law ubermensch free. Abstract chaos snare play truth ultimate good self. God overcome sexuality pious abstract good decieve revaluation aversion good. Virtues chaos overcome society holiest truth.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testi_item" style="border-radius: 13px;">
                                <img src="<?= base_url('assets2/') ?>img/testimonials/inf-1.png" alt="" height="130" width="130">
                                <h4>Advanced Informatics</h4>
                                <ul class="list">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                                <p>Merciful revaluation burying love ultimate value inexpedient ubermensch. Holiest madness victorious morality hope endless christian madness. Love dead fearful transvaluation marvelous. Oneself right ideal abstract endless faith deceptions zarathustra grandeur law ubermensch free. Abstract chaos snare play truth ultimate good self. God overcome sexuality pious abstract good decieve revaluation aversion good. Virtues chaos overcome society holiest truth.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
    <!--================End Team Area =================-->

    <!--================Impress Area =================-->
    <section class="impress_area p_120" width="1728" height="704" style="margin-top: 32px;">
        <div class="container">
            <div class="impress_inner text-center">
                <h2 data-aos="fade-up" data-aos-duration="1800">Masuk Sebagai Dosen</h2>
                <p data-aos="fade-up" data-aos-duration="2000">Merciful revaluation burying love ultimate value inexpedient ubermensch. Holiest madness victorious morality hope endless christian madness. Love dead fearful transvaluation marvelous. Oneself right ideal abstract endless faith deceptions zarathustra grandeur law ubermensch free. Abstract chaos snare play truth ultimate good self. God overcome sexuality pious abstract good decieve revaluation aversion good. Virtues chaos overcome society holiest truth.
                </p>
                <a data-aos="fade-up" data-aos-duration="2200" class="main_btn" href="<!-- <?= base_url('welcome/guru') ?> -->">Masuk Sebagai DOSEN</span></a>
            </div>
        </div>
    </section>
    <!--================End Impress Area =================-->


    <!--================ Start footer Area  =================-->
    <footer class="footer-area p_60">
        <div class="container">
            <div class="row">
                <div class="col-lg-3  col-md-6 col-sm-6" style="margin-top: 32px;">
                    <div class="single-footer-widget tp_widgets">
                        <h6 class="footer_title">Tentang Universitas</h6>
                        <ul class="list">
                            <li><a href="https://unu-jogja.ac.id/home/news.php">Berita Terkini</a></li>
                            <li><a href="#">Beasiswa</a></li>
                            <li><a href="https://pmb.unu-jogja.ac.id/">Penerimaan Mahasiswa Baru</a></li>
                            <li><a href="https://unu-jogja.ac.id">Website Resmi UNU Yogyakarta</a></li>
                        </ul>
                    </div>
                </div>
                <!--             <div class="col-lg-2  col-md-6 col-sm-6" style="margin-top: 32px;">
                <div class="single-footer-widget tp_widgets">
                    <h6 class="footer_title">Masuk</h6>
                    <ul class="list">
                        <li><a href="#" data-toggle="modal" data-target="#exampleModalCenter">Mahasiswa</a></li>
                        <li><a href="<?= base_url('welcome/guru') ?>">Dosen</a></li>
                        <li><a href="<?= base_url('welcome/rektor') ?>">Rektor</a></li>
                        <li><a href="<?= base_url('welcome/orangtua') ?>">Orang Tua</a></li>
                    </ul>
                </div>
            </div>
             -->
                <div class="col-lg-3  col-md-6 col-sm-6" style="margin-top: 32px;">
                    <div class="single-footer-widget tp_widgets">
                        <h6 class="footer_title">Materi</h6>
                        <ul class="list">
                            <li><a href="javaScript:void(0);">Fakultas Teknologi Informasi</a></li>
                            <li><a href="javaScript:void(0);">Fakultas Ekonomi</a></li>
                            <li><a href="javaScript:void(0);">Fakultas Industri Halal</a></li>
                            <li><a href="javaScript:void(0);">Fakultas Ilmu Pendidikan</a></li>
                            <li><a href="javaScript:void(0);">Fakulats Dirosah Islamiyah</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3  col-md-6 col-sm-6" style="margin-top: 32px;">
                    <div class="single-footer-widget tp_widgets">
                        <h6 class="footer_title">Tentang Developer</h6>
                        <ul class="list">
                            <li>Perfectionist Web Developer with one years of experience as a Web Developer and Web Designer. Skilled at Designing and developing Websites. Excellent written and oral communication skills; capable of explaining complex software issues in easy-to-understand terms.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6" style="margin-top: 32px;">
                    <h4 class="footer_title">Tentang Learntara</h4>
                    <p>
                        Web Edukasi Open Source yang dibuat oleh team Advanced Informatics. Learntara adalah Web edukasi yang dilengkapi video, materi dan sistem pembelajaran yang tersedia secara online. Learntara dibuat ditujukan agar para mahasiswa dan dosen dapat terus belajar dan mengajar dimana saja dan kapan saja.
                    </p>
                </div>
            </div>
            <div class="row footer-bottom d-flex justify-content-between align-items-center" style="padding-bottom: 32px;">
                <p class="col-lg-8 col-md-8 footer-text m-0">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | This template is made with <span class="text-danger"> &#10084;</span> by
                    <a href="https://colorlib.com" target="_blank">Colorlib</a> <br> Learntara is made with <span class="text-danger"> &#10084;</span> by <a href="https://github.com/syauqi">Advanced Informatics</a> with MIT License
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
                <div class="col-lg-4 col-md-4 footer-social" pstyl>
                    <a href="https://web.facebook.com/UNUYogyakarta"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/unujogja/"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.youtube.com/channel/UC-gcEavTJ8NNcYFWAmHWD0w/featured"><i class="fa fa-youtube"></i></a>
                    <!-- <a href="https://twitter.com/syaaauqi"><i class="fa fa-twitter"></i></a> -->
                    <!-- <a href="https://dribbble.com/syaufy"><i class="fa fa-dribbble"></i></a> -->
                    <!-- <a href="https://www.behance.net/syaufy"><i class="fa fa-behance"></i></a> -->
                    <!-- <a href="https://www.github.com/syauqi"><i class="fa fa-github"></i></a> -->
                </div>
            </div>
        </div>
    </footer>
    <!--================ End footer Area  =================-->


    <!-- Start Login Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 13px;">
                <div class="modal-header">
                    <h2 class="modal-title text-dark font-weight-bold" style="color:#212529 !important;" id="exampleModalCenterTitle">
                        Masuk Sekarang</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <div class="modal-body">
                    <div class="container-fluid">
                        <br>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <img src="<?= base_url('assets2/'); ?>img/modal-login-2.png" class="img-fluid img-responsive mx-auto " style="height: 350px;">
                            </div>
                            <div class=" col-md-6">

                                <?php
                                if ($this->session->flashdata()) {
                                    echo "<div class='alert alert-danger alert-dismissible fade show'>
                        " . $this->session->flashdata("error-login") . "
                    </div>";
                                }
                                ?>


                                <form action="login/ceklogin" method="post">
                                    <div class="form-group">
                                        <label class="label-font" for="
                                        exampleFormControlInput1">
                                            Email</label>

                                        <?php $has_error = validation_errors() ? "has-error" : ""; ?>
                                        <input type="text" value="<?= set_value('email'); ?>" class="form-control" name="userid" autocomplete="off" id="userid" placeholder="Masukan email disini..." required="required" style="border-radius: 13px;">
                                        <?= form_error("userid"); ?>
                                        <small class="text-danger"></small>
                                    </div>
                                    <div class="form-group">
                                        <label class="label-font" for="
                                        exampleFormControlInput1">
                                            Password</label>
                                        <input type="password" name="password" class="form-control" id="password" placeholder="Masukan password disini..." required="required" style="border-radius: 13px;">
                                        <?= form_error("password"); ?>
                                        <small class="text-danger"></small>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                        <label class="form-check-label" for="defaultCheck1">
                                            Ingat saya.
                                        </label>
                                    </div>
                                    <p class="terms">Dengan login anda
                                        menyetujui
                                        <i>privasi dan persyaratan ketentuan
                                            hukum kami </i> .
                                        belum punya akun? daftar <a href=" <?= base_url('user/registration') ?>">
                                            disini.</a>
                                    </p>
                                    <button class="btn btn-block font-weight-bold" style="background-color: #4dbf1c;color:white;font-size:18px; border-radius: 13px;">Masuk</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Login Modal -->



    <script src="<?= base_url('assets2/') ?>js/stellar.js"></script>
    <script src="<?= base_url('assets2/') ?>vendors/lightbox/simpleLightbox.min.js"></script>
    <!-- <script src="<?= base_url('assets/') ?>vendors/nice-select/js/jquery.nice-select.min.js"></script> -->
    <script src="<?= base_url('assets2/') ?>vendors/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="<?= base_url('assets2/') ?>vendors/isotope/isotope.pkgd.min.js"></script>
    <script src="<?= base_url('assets2/') ?>vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?= base_url('assets2/') ?>vendors/popup/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url('assets2/') ?>js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url('assets2/') ?>vendors/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?= base_url('assets2/') ?>vendors/counter-up/jquery.counterup.js"></script>
    <script src="<?= base_url('assets2/') ?>js/mail-script.js"></script>
    <script src="<?= base_url('assets2/') ?>js/theme.js"></script>
    <script>
        var animateButton = function(e) {
            e.preventDefault;
            e.target.classList.remove('animate');
            e.target.classList.add('animate');
            setTimeout(function() {
                e.target.classList.remove('animate');
            }, 700);
        };

        var bubblyButtons = document.getElementsByClassName("bubbly-button");

        for (var i = 0; i < bubblyButtons.length; i++) {
            bubblyButtons[i].addEventListener('click', animateButton, false);
        }
    </script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>

</html>