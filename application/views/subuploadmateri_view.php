<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbaradmin'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card">
					<div class="card-body">
						<h2 class="card-title">Buat Soal dan Upload Materi </h2>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="padding-20">
								<ul class="nav nav-tabs" id="myTab2" role="tablist">
									<li class="nav-item" style=" display: block; margin-left: auto; margin-right: 0;">
										<a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">Upload Pdf</a>
									</li>
									<li class="nav-item" style=" display: block; margin-left: 0; margin-right: auto;">
										<a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Buat Materi Diskusi</a>
									</li>
								</ul>
								<div class="tab-content tab-bordered" id="myTab3Content">
									<div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
										<form method="POST" action="subuploadmateri/do_upload" enctype="multipart/form-data" class="needs-validation">
											<input type="hidden" name="f" id="f" value="<?php echo $this->session->flashdata('berhasil'); ?>">
											<div class="row">
												<div class="card-body">
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Program Studi</label>
														<div class="col-sm-12 col-md-7">
															<select class="form-control selectric" name="matakuliah" id="matakuliah">
																<option disabled selected>Pilih</option>
																<?php
																$namamk = $this->db->query("select * from tblmatakuliah");
																foreach ($namamk->result() as $row) {
																	echo "<option data-prodi='$row->prodi' data-sem='$row->semester' value='" . $row->kodemk . "'>" . $row->namamk . " | " . $row->prodi . " - " . $row->semester . "</option>";
																}
																?>
															</select>
															<?php echo form_error('matakuliah'); ?>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Judul Materi</label>
														<div class="col-sm-12 col-md-7">
															<input type="text" name="judulmateri" id="judulmateri" class="form-control">
															<?php echo form_error('judulmateri'); ?>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Upload File</label>
														<div class="col-sm-12 col-md-7">
															<div id="mydropzone" class="dropzone">
																<div class="fallback">
																	<label for="upload">Choose File</label>
																	<input type="file" name="upload" id="upload" accept=".pdf" />
																</div>
																<?php echo form_error('upload'); ?>
															</div>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
														<div class="col-sm-6 col-md-7">
															<button class="btn btn-primary" type="submit" id="uploadmateri">Tambah Materi</button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
										<form method="POST" id="myform" name="myform" action="subbuatdiskusi/simpan" enctype="multipart/form-data" class="needs-validation">
											<input type="hidden" name="f" id="f" value="<?php echo $this->session->flashdata('berhasil'); ?>">
											<div class="row">
												<div class="card-body">
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Program Studi</label>
														<div class="col-sm-12 col-md-7">
															<select class="form-control selectric" name="matakuliah" id="matakuliah">
																<option disabled selected>Pilih</option>
																<?php
																$namamk = $this->db->query("select * from tblmatakuliah");
																foreach ($namamk->result() as $row) {
																	echo "<option data-prodi='$row->prodi' data-sem='$row->semester' value='" . $row->kodemk . "'>" . $row->namamk . " | " . $row->prodi . " - " . $row->semester . "</option>";
																}
																?>
															</select>
															<?php echo form_error('matakuliah'); ?>
														</div>
													</div>
													<input type="hidden" name="prodi" id="prodi">
													<input type="hidden" name="semester" id="semester">
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Judul Materi</label>
														<div class="col-sm-12 col-md-7">
															<input type="text" name="judulmateri" id="judulmateri" class="form-control">
															<?php echo form_error('judulmateri'); ?>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Upload File</label>
														<div class="col-sm-12 col-md-7">
															<textarea class="summernote-simple" name="file" id="upload" accept=".pdf"></textarea>
															<?php echo form_error('file'); ?>
														</div>
													</div>
													<div class="form-group row mb-4">
														<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
														<div class="col-sm-6 col-md-7">
															<button class="btn btn-primary" type="submit" id="simpan">Tambah Materi</button>
															<button type="reset" onclick="window.location.reload()" class="btn btn-danger">reset</button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>

	<?php $this->load->view('components/foot'); ?>
	<?php $this->load->view('components/jsfoot2'); ?>
	<script>
		$(document).ready(function() {
			flash = $('#f').attr('value');
			if (flash == "Materi ter-upload.") {
				var divMessage = "<div class='alert alert-success'>" +
					"<strong>Berhasil! </strong> <span>" + flash + "</span>" +
					"</div>";
				$(divMessage)
					.prependTo(".ini")
					.delay(2000)
					.slideUp("slow");
			}
			$('#matakuliah').on('change', function() {
				// alert('asd');
				prodi = $(this).find(':selected').data('prodi');
				semester = $(this).find(':selected').data('sem');
				console.log(prodi, semester);
				$("#prodi").val(prodi);
				$("#semester").val(semester);
			});
		});
	</script>
	<script>
		var editor = CKEDITOR.replace('file');
		CKFinder.setupCKEditor(editor);
	</script>

	</body>

	</html>