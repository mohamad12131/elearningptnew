<base href="<?= base_url(); ?>">
<?php $this->load->view('components/head2'); ?>
<?php $this->load->view('components/navbardosen'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;"><span class="icon-briefcase"></span>
				Buat Soal<small> Essai ataupun pilihan berganda</small></h1>
		</div>
		<hr>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="quick-actions_homepage offset2">
					<ul class="quick-actions">
						<li class="bg_lo span3"> <a href="dosen/subuploadmateri"> <i class="icon-book"></i>Upload Materi PDF </a> </li>
						<li class="bg_lg span3"> <a href="dosen/subbuatdiskusi"> <i class="icon-th-list"></i> Buat Materi Diskusi</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<hr>
</div>






<?php $this->load->view('components/foot'); ?>

<?php $this->load->view('components/jsfoot2'); ?>

</body>

</html>