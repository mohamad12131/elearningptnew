<?php $this->load->view('components/head2'); ?>
<base href="<?= base_url() ?>">
<?php $this->load->view('components/navbaradmin'); ?>
<!-- Main Content -->
<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="card" style="width:100%;">
				<div class="card-body">
					<h2 class="card-title">Management Data Mahasiswa </h2>
					<hr>
					<a href="<?= base_url('admin/add_mhs') ?>" class="btn btn-outline-primary">Tambah
						Data Mahasiswa ⭢</a>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover" id="save-stage" style="width:100%;float:right;">
									<thead>
										<tr>
											<th>NIM</th>
											<th>Nama</th>
											<th>Jenis Kelamin</th>
											<th>Prodi</th>
											<th>Semester</th>
											<th>Kelas</th>
										</tr>
									</thead>
									<tbody>
										<?php

										foreach ($user as $u) {
										?>
											<tr>

												<th scope="row">
													<?php echo $u->nim ?>
												</th>

												<td>
													<?php echo $u->namamhs ?>
												</td>
												<td>
													<?php echo $u->jekel ?>
												</td>
												<td>
													<?php echo $u->prodi ?>
												</td>
												<td>
													<?php echo $u->semester ?>
												</td>
												<td>
													<?php echo $u->kelas ?>
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
	</section>
</div>

<?php if ($this->session->flashdata('success-edit')) : ?>
	<script>
		Swal.fire({
			icon: 'success',
			title: 'Data Guru Telah Dirubah!',
			text: 'Selamat data berubah!',
			showConfirmButton: false,
			timer: 2500
		})
	</script>
<?php endif; ?>

<?php if ($this->session->flashdata('user-delete')) : ?>
	<script>
		Swal.fire({
			icon: 'success',
			title: 'Data Guru Telah Dihapus!',
			text: 'Selamat data telah Dihapus!',
			showConfirmButton: false,
			timer: 2500
		})
	</script>
<?php endif; ?>

<?php $this->load->view('components/foot'); ?>
<?php $this->load->view('components/jsfoot2'); ?>
<script type="text/javascript">
	function readURL(input) {
		$('#previewLoad').show();
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('.image_preview').html('<img src="' + e.target.result + '" alt="' + reader.name +
					'" class="img-thumbnail" width="100" height="75"/>');
			}

			reader.readAsDataURL(input.files[0]);
			$('#previewLoad').hide();
		}
	}

	function reset() {
		$('#image').val("");
		$('.image_preview').html("");
	}
</script>
</body>

</html>