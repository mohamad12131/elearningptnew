<?php $this->load->view('components/head2'); ?>
<base href="<?= base_url() ?>">
<?php $this->load->view('components/navbaradmin'); ?>
<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;">Dashboard</h1>
		</div>
		<div class="container">
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span3 offset1">
						<style>
							.input {
								margin-top: 12px;
								width: 250px;
							}

							.select {
								margin-top: 12px;
							}
						</style>
						<?php
						if ($this->session->userdata('islogin') == 'masukmhs') {
							$nim = $this->session->userdata('ses_id');
							echo "<form action='hmahasiswa/profil/ganti' method='POST'>";
							echo "<input type='hidden' name='nim' value='$nim'>";
						} else {
							$userid = $this->session->userdata('ses_id');
							echo "<form action='profil/ganti' method='POST'>";
							echo "<input type='hidden' name='userid' value='$userid'>";
						}
						?>
						<div class="control-group
                            <?= form_error("password-lama") ? "has-error" : "" ?>">
							<label for="password-lama">Password Lama</label>
							<input type="password" id="password-lama" class="form-control" name="password-lama" value='<?= set_value("password-lama"); ?>'>
							<?= form_error("password-lama"); ?>
						</div>
						<div class="control-group
                            <?= form_error("password-baru") ? "has-error" : "" ?>">
							<label for="password-baru">Password Baru</label>
							<input type="password" id="password-baru" class="form-control" name="password-baru" value='<?= set_value("password-baru"); ?>'>
							<?= form_error("password-baru"); ?>
						</div>
						<div class="control-group
                            <?= form_error("konfirm") ? "has-error" : "" ?>">
							<label for="konfirm">Konfirm Password Baru</label>
							<input type="password" id="konfirm" class="form-control" name="konfirm" value='<?= set_value("konfirm"); ?>'>
							<?= form_error("konfirm"); ?>
						</div>
						<button class="btn btn-success">
							<span class="glyphicon glyphicon-floppy-disk"></span>
							Simpan
						</button>
						<a href="profil" class="btn btn-danger">
							<span class="glyphicon glyphicon-remove"></span>
							Batal
						</a>
						</form>
					</div>
				</div>
			</div>
		</div>
</div>

<?php $this->load->view('components/foot'); ?>
<?php $this->load->view('components/jsfoot2'); ?>
<script>

</script>
</body>

</html>