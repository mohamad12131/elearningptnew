<!-- Menggunakan CSS dan JSnya Okta -->

<!-- General JS Scripts -->
<script src="<?= base_url('assets2/') ?>assets/js/app.min.js"></script>
<script src="<?= base_url('assets2/') ?>assets/js/scripts.js"></script>
<!-- JS Libraies -->
<script src="<?= base_url('assets2/') ?>assets/bundles/apexcharts/apexcharts.min.js"></script>
<script src="<?= base_url('assets2/') ?>assets/bundles/summernote/summernote-bs4.js"></script>


<script src="<?= base_url('assets2/') ?>assets/bundles/chartjs/chart.min.js"></script>
<script src="<?= base_url('assets2/') ?>assets/bundles/jquery.sparkline.min.js"></script>
<script src="<?= base_url('assets2/') ?>assets/bundles/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?= base_url('assets2/') ?>assets/bundles/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?= base_url('assets2/') ?>assets/bundles/jqvmap/dist/maps/jquery.vmap.indonesia.js"></script>
<!-- Page Specific JS File -->
<script src="<?= base_url('assets2/') ?>assets/js/page/widget-chart.js"></script>
<script src="<?= base_url('assets2/') ?>assets/js/custom.js"></script>
<script src="<?= base_url('assets2/') ?>assets/js/page/index.js"></script>
<!-- Template JS File -->
<!-- Custom JS File -->