<!-- Menggunakan CSS dan JSnya Okta -->

  <!-- General JS Scripts -->
  <script src="<?= base_url('assets/') ?>assets2/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?= base_url('assets/') ?>assets2/bundles/apexcharts/apexcharts.min.js"></script>
  <script src="<?= base_url('assets/') ?>assets2/bundles/summernote/summernote-bs4.js"></script>


  <script src="<?= base_url('assets/') ?>assets2/bundles/chartjs/chart.min.js"></script>
  <script src="<?= base_url('assets/') ?>assets2/bundles/jquery.sparkline.min.js"></script>
  <script src="<?= base_url('assets/') ?>assets2/bundles/jqvmap/dist/jquery.vmap.min.js"></script>
  <script src="<?= base_url('assets/') ?>assets2/bundles/jqvmap/dist/maps/jquery.vmap.world.js"></script>
  <script src="<?= base_url('assets/') ?>assets2/bundles/jqvmap/dist/maps/jquery.vmap.indonesia.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?= base_url('assets/') ?>assets2/js/page/widget-chart.js"></script>
  <script src="<?= base_url('assets/') ?>assets2/js/page/index.js"></script>
  <!-- Template JS File -->
  <script src="<?= base_url('assets/') ?>assets2/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?= base_url('assets/') ?>assets2/js/custom.js"></script>