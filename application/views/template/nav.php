<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="<?= base_url('assets2/') ?>img/unu-1.png" type="image/png">
    <!-- Title -->
    <title>
        Hai,
        <?= $this->session->userdata("ses_nama"); ?>
        <?php if ($this->session->userdata("islogin") == 'masukmhs') { ?>
        <?php } ?>
    </title>
    <!-- Bootstrap CSS -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/linericon/style.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/animate-css/animate.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>vendors/popup/magnific-popup.css">
    <!-- Main css -->
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/style.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/user_style.css">
    <link rel="stylesheet" href="<?= base_url('assets2/') ?>css/responsive.css">
    <!-- Fonts -->
    <!-- Template CSS -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets2/') ?>stisla-assets/css/style.css"> -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets2/') ?>stisla-assets/css/components.css"> -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <!-- Library -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.4/dist/sweetalert2.all.min.js"></script>

    <!-- Dosen Link -->

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="<?= base_url('assets2') ?>/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Page Vendors Styles -->

    <!--begin:: Global Mandatory Vendors -->
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets2') ?>/assets/vendors/custom/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url('assets2') ?>/assets/demo/demo7/base/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <!--end::Layout Skins -->
    <!-- <link rel="shortcut icon" href="<?= base_url('assets2') ?>/img/favicon.png" /> -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.0/dist/sweetalert2.all.min.js"></script>

</head>

<body style="overflow-x:hidden;background-color:#fbf9fa">



    <!-- Start Navigation Bar -->
    <div class="container">
        <header class="header_area" style="background-color: white !important; ">
            <div class="main_menu">
                <nav class="navbar navbar-expand-lg navbar-light">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="<?= base_url('welcome') ?>"><img src="<?= base_url('assets2/') ?>img/unu-2.png" alt="" style="width: 225px; height: 55px; margin-left: 32px;"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent" style="margin-right: 32px;">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <li class="nav-item ">
                                <a class="nav-link" href="<?= base_url('user/profil') ?>">Profil
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="<?= base_url('hmahasiswa/user') ?>">Beranda</a>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('hmahasiswa/materimhs') ?>">Materi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('hmahasiswa/absensimhs') ?>">Absensi</a>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('hmahasiswa/soalmhs') ?>">Tugas</a>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('hmahasiswa/user/ujian') ?>">Nilai</a>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('hmahasiswa/materimhs') ?>">Forum Diskusi</a>
                            </li>
                            <li class=" nav-item " style="color: red;">
                                <a class=" nav-link text-danger" href="<?= base_url('welcome/logout') ?>">Keluar</a>
                            </li>
                            </li>
                            </li>
                            </li>
                            </li>
                        </ul>
                    </div>
            </div>
            </nav>
    </div>
    </header>